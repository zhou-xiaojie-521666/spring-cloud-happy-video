package com.zhou.apis.user;


import com.zhou.model.common.dtos.ResponseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient("happy-user")
public interface UserClient {
    @PostMapping("/api/getCoverById/{id}")
    public ResponseResult saveArticle(@PathVariable Long id);
}
