package com.zhou.apis.schedule;


import com.zhou.apis.article.fallback.IArticleClientFallback;
import com.zhou.model.vedio.pojos.Video;
import com.zhou.model.common.dtos.ResponseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Primary;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
@Primary
@FeignClient(value = "happy-user",fallback = IArticleClientFallback.class)
public interface IArticleClient {

    @PostMapping("/api/getCoverById/{id}")
    public ResponseResult saveArticle(@PathVariable Long id);
}

