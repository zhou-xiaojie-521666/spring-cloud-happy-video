package com.zhou.apis.schedule;

import com.zhou.apis.article.fallback.IArticleClientFallback;
import com.zhou.apis.article.fallback.ISearchClientFallback;
import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.model.vedio.dto.SearchArticleVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

import java.io.IOException;

@FeignClient(value = "happy-search",fallback = ISearchClientFallback.class)
public interface ISearchFeignClient {

    @PostMapping("/api/search/insertElastic")
    public ResponseResult insertElastic(SearchArticleVo searchArticleVo) throws IOException;


}
