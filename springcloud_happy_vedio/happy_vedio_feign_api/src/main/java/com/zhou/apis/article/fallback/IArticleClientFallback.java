package com.zhou.apis.article.fallback;

import com.zhou.apis.schedule.IArticleClient;
import com.zhou.model.vedio.pojos.Video;
import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.model.common.enums.AppHttpCodeEnum;
import org.springframework.stereotype.Component;

/**
 * feign失败配置
 * @author itzhou
 */
@Component
public class IArticleClientFallback implements IArticleClient {

    @Override
    public ResponseResult saveArticle(Long id) {
        return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR,"获取数据失败");
    }
}