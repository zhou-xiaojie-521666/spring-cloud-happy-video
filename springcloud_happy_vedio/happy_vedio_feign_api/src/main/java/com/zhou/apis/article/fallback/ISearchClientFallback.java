package com.zhou.apis.article.fallback;

import com.zhou.apis.schedule.IArticleClient;
import com.zhou.apis.schedule.ISearchFeignClient;
import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.model.common.enums.AppHttpCodeEnum;
import com.zhou.model.vedio.dto.SearchArticleVo;
import com.zhou.model.vedio.pojos.Video;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * feign失败配置
 * @author itzhou
 */
@Component
public class ISearchClientFallback implements ISearchFeignClient {

    @Override
    public ResponseResult insertElastic(SearchArticleVo searchArticleVo) throws IOException {
        return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR,"获取数据失败");
    }
}