package com.zhou.es.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhou.es.pojo.SearchArticleVo;
import com.zhou.model.vedio.pojos.Video;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface VideoMapper extends BaseMapper<Video> {

    public List<SearchArticleVo> loadArticleList();

}
