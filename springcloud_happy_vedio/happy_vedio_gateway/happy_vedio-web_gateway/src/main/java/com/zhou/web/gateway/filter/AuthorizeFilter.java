package com.zhou.web.gateway.filter;


import com.zhou.web.gateway.util.AppJwtUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class AuthorizeFilter implements Ordered, GlobalFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //1.获取request和response对象
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();

        //2.判断是否是登录 发送验证码
        if(request.getURI().getPath().contains("/login")||
                request.getURI().getPath().contains("/checkCode")||
                request.getURI().getPath().contains("/sendEmailCode")||
                request.getURI().getPath().contains("/register")||
                request.getURI().getPath().contains("/getAllVideo")||
                request.getURI().getPath().contains("/getImage")||
                request.getURI().getPath().contains("/ts/getVideoInfo")||
                request.getURI().getPath().contains("/test")||
                request.getURI().getPath().contains("/associate")||
                request.getURI().getPath().contains("/getUserInfoById")||
                request.getURI().getPath().contains("/getCommmentById")||
                request.getURI().getPath().contains("/getLikesCount")||
                request.getURI().getPath().contains("/correlationSearch")||
                request.getURI().getPath().contains("/getFollowingList")||
                request.getURI().getPath().contains("/getFollowingCount")||
                request.getURI().getPath().contains("/getFansCount")||
                request.getURI().getPath().contains("/getChatHistory")||
                request.getURI().getPath().contains("/getDanumList")

        ){
            //放行
            return chain.filter(exchange);
        }

        //3.获取token
        String token = request.getHeaders().getFirst("token");

        //4.判断token是否存在
        if(StringUtils.isBlank(token)){
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }
        //5.判断token是否有效
        try {
            Claims claimsBody = AppJwtUtil.getClaimsBody(token);
            //是否是过期

            int result = AppJwtUtil.verifyToken(claimsBody);
            if(result == 1 || result  == 2){
                response.setStatusCode(HttpStatus.UNAUTHORIZED);
                return response.setComplete();
            }

            //获取用户信息
            Object userId = claimsBody.get("id");

            //放到请求头中
            ServerHttpRequest serverHttpRequest = request.mutate().headers(httpHeaders -> {
                httpHeaders.add("userId", userId + "");
            }).build();


            //重置请求
            exchange.mutate().request(serverHttpRequest);

        }catch (Exception e){
            e.printStackTrace();
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }
        //6.放行
        return chain.filter(exchange);
    }

    /**
     * 优先级设置  值越小  优先级越高
     * @return
     */
    @Override
    public int getOrder() {
        return 0;
    }
}
