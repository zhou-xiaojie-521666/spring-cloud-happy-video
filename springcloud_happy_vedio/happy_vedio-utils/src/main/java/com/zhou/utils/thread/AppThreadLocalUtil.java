package com.zhou.utils.thread;

import com.zhou.model.user.pojos.UserInfo;

public class AppThreadLocalUtil {

    private final static ThreadLocal<UserInfo> AP_USER_THREAD_LOCAL=new ThreadLocal<>();

    //存入线程，从线程中获取
    public static void setUser(UserInfo apUser){
        AP_USER_THREAD_LOCAL.set(apUser);
    }

    public static UserInfo getUser(){
        return AP_USER_THREAD_LOCAL.get();
    }

    public static void clear(){
        AP_USER_THREAD_LOCAL.remove();
    }
}
