package com.zhou.utils;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;


public class StringTools {
    public static Boolean isEmpty(String str){
        if (null==str||"".equals(str.trim())||"null".equals(str)){
            return true;
        }else {
            return false;
        }
    }
    public static final String getRandomString(Integer count){
        return RandomStringUtils.random(count,true,true);
    }

    public static final String getRandomNumber(Integer count){
        return RandomStringUtils.random(count,false,true);
    }

    public static String encodeMd5(String souyceStr){
        return StringTools.isEmpty(souyceStr)?null: DigestUtils.md5Hex(souyceStr);
    }

    public static String getFileSuffix(String fileName){
        return fileName.substring(fileName.lastIndexOf("."));
    }
    /**
     * 转义，防止有人传一串js
     */
    public static String eecpapeHtml(String content){
        if (StringTools.isEmpty(content)){
            return content;
        }
        content=content.replace("<","&lt;");
        content=content.replace("\n","<br>");
        return content;
    }

    /**
     * 给图片的名称进行截断
     */
    public static String getFileName(String fileName ){
        return fileName.substring(0,fileName.lastIndexOf("."));
    }

    public static boolean pathIsOk(String path) {
        if (StringTools.isEmpty(path)) {
            return true;
        }
        if (path.contains("../") || path.contains("..\\")) {
            return false;
        }
        return true;
    }


    public static String getFileNameNoSuffix(String fileName) {
        Integer index = fileName.lastIndexOf(".");
        if (index == -1) {
            return fileName;
        }
        fileName = fileName.substring(0, index);
        return fileName;
    }
}
