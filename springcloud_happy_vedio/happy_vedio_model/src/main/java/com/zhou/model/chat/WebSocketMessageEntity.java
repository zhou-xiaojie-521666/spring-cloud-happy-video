package com.zhou.model.chat;

import com.alibaba.fastjson.JSON;

public class WebSocketMessageEntity {
    private String fromId;

    private String toId;

    private String msg;

    private int action;

    public WebSocketMessageEntity() {
    }

    public WebSocketMessageEntity(String fromId, String toId, String msg, int action) {
        this.fromId = fromId;
        this.toId = toId;
        this.msg = msg;
        this.action = action;
    }

    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    public String getToId() {
        return toId;
    }

    public void setToId(String toId) {
        this.toId = toId;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
