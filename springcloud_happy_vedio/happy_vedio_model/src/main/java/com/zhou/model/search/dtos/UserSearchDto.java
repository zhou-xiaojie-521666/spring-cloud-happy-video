package com.zhou.model.search.dtos;

import lombok.Data;

import java.util.Date;


@Data
public class UserSearchDto {

    /**
    * 搜索关键字
    */
    String searchWords;

    /**
     * 用户或者视频
     */
    String board;


    /**
     * 筛选，最新发布，综合
     */
    String screen;
}