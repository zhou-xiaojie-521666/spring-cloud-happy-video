package com.zhou.model.common.dtos;

import lombok.Data;

@Data
public class LoginDto {
    /**
     * 邮箱
     */
    private String email;

    /**
     * 密码
     */
    private String password;

    private String code;

}
