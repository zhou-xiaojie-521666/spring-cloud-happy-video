package com.zhou.model.common.dtos;

import lombok.Data;

@Data
public class DownOrUpDto {
    private Integer id;

    /**
     * 0下架
     * 1上架
     */
    private Short enable;
}
