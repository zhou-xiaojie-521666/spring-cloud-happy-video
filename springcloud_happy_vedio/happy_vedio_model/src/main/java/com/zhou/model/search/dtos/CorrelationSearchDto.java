package com.zhou.model.search.dtos;

import lombok.Data;

@Data
public class CorrelationSearchDto {

    /**
     * 搜索关键字
     */
    String searchWords;
}
