package com.zhou.model.vedio.pojos;

import lombok.Data;
import lombok.EqualsAndHashCode;
import com.zhou.model.BaseEntity;

/**
 * 视频点赞表
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class VideoStar extends BaseEntity {

    private static final long serialVersionUID = 1L;


    private Long videoId;

    private Long userId;


}
