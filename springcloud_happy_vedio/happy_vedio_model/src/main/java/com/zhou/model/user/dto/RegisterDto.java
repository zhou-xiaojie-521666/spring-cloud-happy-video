package com.zhou.model.user.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterDto {
    private String email;

    /**
     * 邮箱验证码
     */
    private String emailCode;

    private String password;

    /**
     * 验证码
     */
    private String checkCode;

}
