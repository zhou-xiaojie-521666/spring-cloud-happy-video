package com.zhou.model.search.vos;

import lombok.Data;

import java.util.Date;

@Data
public class SearchVideoVo {

    // 文章id
    private Long id;
    // 文章标题
    private String title;
    // 文章发布时间
    private Date gmtCreateTime;

    // 作者id
    private Long userId;


    private String description;

}