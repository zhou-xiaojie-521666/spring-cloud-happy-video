package com.zhou.model.enums;

public enum UserStatusEnum {
    DISABLE(0, "启用"),
    ABLE(1, "禁用");


    private Integer status;
    private String desc;

    UserStatusEnum(Integer status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public Integer getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }
}