package com.zhou.model.comment;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("video_comment")
public class VideoComment {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private Long videoId;

    private Long userId;

    private String content;

    private Long replyId;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date postTime;

    @TableField(exist = false)
    private String commentCover;

    @TableField(exist = false)
    private String userCover;

    @TableField(exist = false)
    private String nickName;

    @TableField(exist = false)
    private List<VideoComment> replyCommentList;

    @TableField(exist = false)
    private Boolean haveReply;

}
