package com.zhou.model.vedio.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zhou.model.vo.UserVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.zhou.model.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.ObjectUtils;

import javax.validation.constraints.NotBlank;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("video")
public class Video {

    private static final long serialVersionUID = 1L;

    @TableId(value = "pid", type = IdType.AUTO)
    private Long pid;

    private Long id;

    private String fileName;

    private Long fileSize;

    private String fileMd5;

    private String title;

    private String description;

    private String url;

    private Integer userId;

    @TableField(exist = false)
    private String userCover;

    @TableField(exist = false)
    private String nickName;


    @TableField(exist = false)
    private Long commentCount;

    private String tagList;

    /**
     * 公开/私密，0：公开，1:仅自己可见 2：私密，默认为0
     */
    private Integer open;

    private String cover;

    private Integer auditQueueStatus;

    // 审核状态:通过,未通过,审核中
    private Integer auditStatus;

    // TODO 审核状态的消息，当前嵌套在这里，应该有一个审核表?
    // private String msg;

    // 点赞数
    private Long startCount;

    // 分享数
    private Long shareCount;

    // 浏览次数
    private Long historyCount;

    // 收藏次数
    private Long favoritesCount;

    // 视频时长
//    private String duration;

    // 视频分类
//    @TableField(exist = false)
//    private String videoType;

//    private String labelNames;


    private Integer isDeleted;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date gmtCreated;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date gmtUpdated;

    private String postTime;

    private String detailPostTime;

    private Integer status;


//    // 关联的用户
//    @TableField(exist = false)
//    private UserVO user;
//
//    // 关联分类名称
//    @TableField(exist = false)
//    private String typeName;
//
//    // 是否点赞
//    @TableField(exist = false)
//    private Boolean start;


    // 是否收藏
//    @TableField(exist = false)
//    private Boolean favorites;
//
//    // 是否关注
//    @TableField(exist = false)
//    private Boolean follow;
//
//    // 用户昵称
//    @TableField(exist = false)
//    private String userName;
//
//    // 审核状态名称
//    @TableField(exist = false)
//    private String auditStateName;
//
//    // 是否公开
//    @TableField(exist = false)
//    private String openName;





}

