package com.zhou.model.chat.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Danmu {
    @TableId(type = IdType.AUTO)
    private Long id;     // 弹幕ID
    private Long vid;    // 视频ID
    private Long uid;    // 用户ID
    private String content; // 弹幕内容

    private Double timePoint;   // 弹幕在视频中的时间点位置（秒）
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "Asia/Shanghai")
    private Date createDate;    // 弹幕发送日期时间
}
