package com.zhou.model.comment;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("comment_cover")
public class CommentCover {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String cover;

    private Long commentId;
}
