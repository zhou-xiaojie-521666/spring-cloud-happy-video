package com.zhou.model.vedio.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SearchArticleVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long pid;
    // 文章id
    private Long id;
    // 文章标题
    private String title;
    // 文章发布时间
    private Date gmtCreated;
    // 作者id
    private Long userId;

    private String nickName;
    //文章内容
    private String description;

    private Long startCount;

    private Long shareCount;

    private Long favoritesCount;

    private Integer isDeleted;



}