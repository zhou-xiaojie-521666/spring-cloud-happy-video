package com.zhou.model.chat.enums;

public enum MessageTypeEnums {
    COMMON(0,"普通信息"),
    SHARE(1, "转发信息"),
    INFORM(2, "通知信息");

    private Integer type;

    private String desc;

    MessageTypeEnums(Integer type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
