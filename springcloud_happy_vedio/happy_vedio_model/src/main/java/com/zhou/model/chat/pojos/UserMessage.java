package com.zhou.model.chat.pojos;

import com.baomidou.mybatisplus.annotation.TableField;
import com.zhou.model.user.pojos.UserInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long fromId;

    private String message;

    private Long toId;

    private Date createTime;

    private String image;

    //0：普通信息 1：转发信息 2：通知信息
    private Integer type;

    @TableField(exist = false)
    private String from;

    @TableField(exist = false)
    private String toUserAvatar;

    @TableField(exist = false)
    private String fromUserAvatar;

    @TableField(exist = false)
    private String toUserNickName;

    @TableField(exist = false)
    private String fromUserNickName;

}
