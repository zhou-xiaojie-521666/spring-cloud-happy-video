package com.zhou.model;

public class MqContants {

    /**
     * 交换机
     */
    public static final String VIDEO_EXCHANGE="video.topic";

    /**
     * 信息交换机
     */
    public static final String MESSAGE_EXCHANGE="message.topic";

    /**
     * 监听增改的队列
     */
    public static final  String VIDEO_INSERT_QUEUE="video.insert.queue";

    /**
     * 监听修改的队列
     */
    public static final  String VIDEO_UPDATE_QUEUE="video.update.queue";

    /**
     * 新增或修改的routingkey
     */
    public static final String VIDEO_UPDATE_KEY="video.update";

    /**
     * 监听信息发送的队列
     */
    public static final  String MESSAGE_INSERT_QUEUE="message.insert.queue";

    /**
     * 监听删除
     */
    public static final  String VIDEO_DELETE_QUEUE="video.delete.queue";

    public static final  String MESSAGE_DELETE_QUEUE="message.delete.queue";

    /**
     * 新增或修改的routingkey
     */
    public static final String VIDEO_INSERT_KEY="video.insert";

    /**
     * 信息新增或修改的routingkey
     */
    public static final String MESSAGE_INSERT_KEY="message.insert";

    /**
     * 删除的routingkey
     */
    public static final String VIDEO_DELETE_KEY="video.delete";

    public static final String MESSAGE_DELETE_KEY="message.delete";


    public static final String Behavior_EXCHANGE="behavior.topic";

    public static final  String Behavior_INSERT_QUEUE="behavior.insert.queue";

    public static final  String Behavior_DELETE_QUEUE="behavior.delete.queue";

    public static final String Behavior_INSERT_KEY="behavior.insert";

    public static final String Behavior_DELETE_KEY="behavior.delete";

}
