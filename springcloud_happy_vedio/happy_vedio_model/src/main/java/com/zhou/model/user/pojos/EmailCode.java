package com.zhou.model.user.pojos;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailCode {

    @TableId
    private String email;

    private String code;

    private Date createTime;

    private Integer status;

}