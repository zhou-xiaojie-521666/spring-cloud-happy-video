package com.zhou.model.vedio.enums;

//审核状态
public enum AuditQueueStatusEnum {
    AUDITING(0,"审核中"),
    DISABLE(-1, "禁止发布"),
    ABLE(1, "允许发布");



    private Integer status;
    private String desc;

    AuditQueueStatusEnum(Integer status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public Integer getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }
}