package com.zhou.model.user.pojos;

import lombok.Data;

@Data
public class MenuKey {
    private String title;
    private String href;
    private String image;
}