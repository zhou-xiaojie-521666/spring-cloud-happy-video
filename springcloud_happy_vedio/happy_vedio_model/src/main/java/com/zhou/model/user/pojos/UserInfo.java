package com.zhou.model.user.pojos;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.zhou.model.BaseEntity;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.Set;

/**
 * 用户表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private Integer isDeleted;

    private Date gmtCreated;

    private Date gmtUpdated;

    private String nickName;

    private String email;

    private String password;

    private String description;

    private Integer sex;

    private Integer defaultFavoritesId;

    private String avatar;



}
