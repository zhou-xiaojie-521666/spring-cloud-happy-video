package com.zhou.model.vedio.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.zhou.model.vedio.pojos.Video;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UploadResultDto implements Serializable {
    private Long fileId;
    private Integer status;
    private String url;

    private SearchArticleVo searchArticleVo;


}