package com.zhou.model.vedio.pojos;

import com.zhou.model.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


/**
 * 视频分享表
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class VideoShare {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Long videoId;

    private Long receiveId;

    private Long sendId;

    private Long vid;

    private Long pid;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date gmtCreated;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date gmtUpdated;

}
