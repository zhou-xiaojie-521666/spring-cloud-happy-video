package com.zhou.model.vedio.enums;

public enum UploadStatusEnums {
    UPLOAD_SECONDS(100, "秒传"),
    UPLOADING(101, "上传中"),
    UPLOAD_FINISH(200, "上传完成");

    private Integer code;
    private String desc;

    UploadStatusEnums(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}