package com.zhou.common.exception;

import com.zhou.model.enums.ResponseCodeEnum;

public class BusinessException extends RuntimeException{
    private ResponseCodeEnum codeEnum;

    private Integer code;

    private String message;

    public BusinessException(String message, Throwable e){
        super(message,e);
        this.message=message;
    }
    public BusinessException(String message){
        super(message);
        this.message=message;
    }
    public BusinessException(Throwable e){
        super(e);
    }

    public BusinessException(ResponseCodeEnum codeEnum){
        super(codeEnum.getMsg());
        this.codeEnum=codeEnum;
        this.code=codeEnum.getCode();
        this.message=codeEnum.getMsg();
    }
    public BusinessException(Integer code, String message){
        super(message);
        this.code=code;
        this.message=message;
    }

    public ResponseCodeEnum getResponseCodeEnum() {
        return codeEnum;
    }

    public Integer getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public Throwable fillInstackTrace(){
        return this;
    }

    public void setCodeEnum(ResponseCodeEnum codeEnum) {
        this.codeEnum = codeEnum;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
