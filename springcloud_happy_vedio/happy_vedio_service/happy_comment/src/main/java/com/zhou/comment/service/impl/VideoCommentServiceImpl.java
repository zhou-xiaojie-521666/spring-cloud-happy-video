package com.zhou.comment.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.zhou.comment.config.WebConfig;
import com.zhou.comment.feign.UserClient;
import com.zhou.comment.mapper.CommentCoverMapper;
import com.zhou.comment.mapper.VideoCommentMapper;
import com.zhou.comment.service.VideoCommentService;
import com.zhou.common.exception.BusinessException;
import com.zhou.model.Constants;
import com.zhou.model.comment.CommentCover;
import com.zhou.model.comment.VideoComment;
import com.zhou.model.comment.dto.PostCommentDto;
import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.model.user.pojos.UserInfo;
import com.zhou.utils.StringTools;
import com.zhou.utils.thread.AppThreadLocalUtil;
import io.swagger.models.auth.In;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class VideoCommentServiceImpl extends ServiceImpl<VideoCommentMapper, VideoComment> implements VideoCommentService {

    @Autowired
    private CommentCoverMapper commentCoverMapper;

    @Autowired
    private WebConfig webConfig;

    @Autowired
    private UserClient userClient;


    @Override
    public List<VideoComment> getCommmentById(Long id) {
        LambdaQueryWrapper<VideoComment> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(VideoComment::getVideoId, id);

        // 获取所有评论
        List<VideoComment> allComments = list(queryWrapper);

        // 构建映射关系，从父评论ID到子评论列表
        Map<Long, List<VideoComment>> childrenMap = allComments.stream()
                .filter(c -> c.getReplyId() != 0) // 排除一级评论（即直接评论视频的评论）
                .collect(Collectors.groupingBy(VideoComment::getReplyId));

        // 过滤出一级评论
        List<VideoComment> parentComments = allComments.stream()
                .filter(c -> c.getReplyId().equals(0L))
                .collect(Collectors.toList());

        // 为一级评论设置嵌套的回复列表
        parentComments.forEach(parentComment -> {
            List<VideoComment> nestedReplies = new ArrayList<>();
            buildNestedReplies(nestedReplies, parentComment.getId(), childrenMap);
            parentComment.setReplyCommentList(nestedReplies);
            setUserCoverAndNickName(parentComment);
        });

        return parentComments;
    }
    // 递归构建嵌套回复
    private void buildNestedReplies(List<VideoComment> result, Long parentId, Map<Long, List<VideoComment>> childrenMap) {
        List<VideoComment> children = childrenMap.get(parentId);
        if (children != null) {
            for (VideoComment child : children) {
                result.add(child);
                buildNestedReplies(result, child.getId(), childrenMap);
                setUserCoverAndNickName(child); // 设置每个子评论的封面和昵称
            }
        }
    }

    // 设置用户封面和昵称的方法
    private void setUserCoverAndNickName(VideoComment comment) {
        Long userId = comment.getUserId();
        UserInfo userInfo = userClient.getUserInfoById(userId);

        comment.setUserCover(userInfo.getAvatar());
        comment.setNickName(userInfo.getNickName());

        LambdaQueryWrapper<CommentCover> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CommentCover::getCommentId, comment.getId());
        CommentCover commentCover = commentCoverMapper.selectOne(queryWrapper);

        if (commentCover != null) {
            comment.setCommentCover(commentCover.getCover());
        }

        // 如果这是父评论且有嵌套的回复列表，则也为所有回复设置封面和昵称
        if (comment.getReplyCommentList() != null) {
            comment.getReplyCommentList().forEach(this::setUserCoverAndNickName);
        }
    }

    // 递归获取子评论
    private List<VideoComment> getReplyComments(Long parentId) {
        LambdaQueryWrapper<VideoComment> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(VideoComment::getReplyId, parentId);
        List<VideoComment> replyComments = list(queryWrapper);
        // 为每个子评论设置用户信息和评论封面
        replyComments.forEach(reply -> {
            Long userId = reply.getUserId();
            UserInfo coverById = userClient.getUserInfoById(userId);
            reply.setUserCover(coverById.getAvatar());
            reply.setNickName(coverById.getNickName());
            Long commentId = reply.getId();
            LambdaQueryWrapper<CommentCover> queryWrapper1 = new LambdaQueryWrapper<>();
            queryWrapper1.eq(CommentCover::getCommentId, commentId);
            CommentCover commentCover = commentCoverMapper.selectOne(queryWrapper1);
            if (commentCover != null) {
                reply.setCommentCover(commentCover.getCover());
            }

        });
        return replyComments;
    }


    @Transactional
    @Override
    public void postComment(MultipartFile file, String content, Long videoId, Long replyId) {
        UserInfo user = AppThreadLocalUtil.getUser();
        if (user.getId() == null) {
            throw new BusinessException("暂未登录");
        }
        if (replyId != null) {
            //二级评论
            String path = null;
            VideoComment videoComment = new VideoComment();
            // 假设videoId, userId, content是通过方法参数传递进来的
            videoComment.setId(null); // ID通常由数据库自动生成
            videoComment.setVideoId(videoId);
            videoComment.setUserId(user.getId());
            videoComment.setContent(content);
            videoComment.setReplyId(replyId);
            videoComment.setPostTime(new Date());
            save(videoComment);
            // 处理上传的文件（如果有）
            if (file != null && !file.isEmpty()) {
                String fileName = file.getOriginalFilename();
                String fileExtName = StringTools.getFileSuffix(fileName);
                if (!ArrayUtils.contains(Constants.IMAGE_SUFFIX, fileExtName)) {
                    throw new BusinessException("文件类型错误");
                }
                path = copyFile(file);
                CommentCover commentCover = new CommentCover();
                commentCover.setId(null); // ID通常由数据库自动生成
                commentCover.setCover(path); // 设置封面图片的路径
                commentCover.setCommentId(videoComment.getId()); // 设置关联的评论ID
                commentCoverMapper.insert(commentCover);
            }
        } else {
            String path = null;
            VideoComment videoComment = new VideoComment();
            // 假设videoId, userId, content是通过方法参数传递进来的
            videoComment.setId(null); // ID通常由数据库自动生成
            videoComment.setVideoId(videoId);
            videoComment.setUserId(user.getId());
            videoComment.setContent(content);
            videoComment.setReplyId(0L);
            videoComment.setPostTime(new Date());
            save(videoComment);
            // 处理上传的文件（如果有）
            if (file != null && !file.isEmpty()) {
                String fileName = file.getOriginalFilename();
                String fileExtName = StringTools.getFileSuffix(fileName);
                if (!ArrayUtils.contains(Constants.IMAGE_SUFFIX, fileExtName)) {
                    throw new BusinessException("文件类型错误");
                }
                path = copyFile(file);
                CommentCover commentCover = new CommentCover();
                commentCover.setId(null); // ID通常由数据库自动生成
                commentCover.setCover(path); // 设置封面图片的路径
                commentCover.setCommentId(videoComment.getId()); // 设置关联的评论ID
                commentCoverMapper.insert(commentCover);
            }

        }
    }

    @Override
    public Long getVideoCommentCount(Long id) {
        LambdaQueryWrapper<VideoComment> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(VideoComment::getVideoId, id);
        Long count = (long) count(queryWrapper);
        return count;
    }


    private String copyFile(MultipartFile file) {
        try {
            String fileName = file.getOriginalFilename();
            String fileExtName = StringTools.getFileSuffix(fileName);
            String fileRealName = StringTools.getRandomString(30) + fileExtName;
            String folderPath = webConfig.getProjectFolder() + Constants.FILE_FOLDER_FILE + Constants.FILE_FOLDER_TEMP;
            File folder = new File(folderPath);
            if (!folder.exists()) {
                folder.mkdirs();
            }
            File uploadFile = new File(folderPath + "/" + fileRealName);
            file.transferTo(uploadFile);
//            + "/"
            return "temp" + "/" + fileRealName;
        } catch (Exception e) {
            throw new BusinessException("上传文件失败");
        }
    }
}
