package com.zhou.comment.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhou.comment.mapper.CommentCoverMapper;
import com.zhou.comment.service.CommentCoverService;
import com.zhou.model.comment.CommentCover;
import com.zhou.model.comment.VideoComment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentCoverServiceImpl extends ServiceImpl<CommentCoverMapper,CommentCover> implements CommentCoverService {


}
