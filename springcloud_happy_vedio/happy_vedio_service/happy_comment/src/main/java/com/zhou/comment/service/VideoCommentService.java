package com.zhou.comment.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhou.model.comment.VideoComment;
import com.zhou.model.comment.dto.PostCommentDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public interface VideoCommentService extends IService<VideoComment> {

    List<VideoComment> getCommmentById(Long id);

    void postComment(MultipartFile file, String content, Long videoId, Long replyId);

    Long getVideoCommentCount(Long id);
}
