package com.zhou.comment.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhou.model.comment.CommentCover;
import com.zhou.model.comment.VideoComment;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CommentCoverService extends IService<CommentCover> {

}
