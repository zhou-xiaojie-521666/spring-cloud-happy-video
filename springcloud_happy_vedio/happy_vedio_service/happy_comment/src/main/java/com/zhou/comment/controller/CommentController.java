package com.zhou.comment.controller;


import com.zhou.comment.config.WebConfig;
import com.zhou.comment.service.CommentCoverService;
import com.zhou.comment.service.VideoCommentService;
import com.zhou.common.exception.BusinessException;
import com.zhou.model.Constants;
import com.zhou.model.comment.VideoComment;
import com.zhou.model.comment.dto.PostCommentDto;
import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.utils.StringTools;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/comment")
@Api(value = "评论", tags = "ap_user", description = "评论API")
public class CommentController {

    @Autowired
    private CommentCoverService commentCoverService;

    @Autowired
    private VideoCommentService videoCommentService;

    @Autowired
    private WebConfig webConfig;


    @RequestMapping("/test")
    public String test(){
        return "test";
    }


    @RequestMapping("/getCommmentById/{id}")
    public ResponseResult getCommmentById(@PathVariable Long id){
        List<VideoComment> commmentById = videoCommentService.getCommmentById(id);
        return ResponseResult.okResult(commmentById);
    }

    @RequestMapping("/postComment")
    public ResponseResult postComment(MultipartFile file, String content, Long videoId, Long replyId){
        videoCommentService.postComment(file,content,videoId,replyId);
        return ResponseResult.okResult("发布成功");
    }

    @RequestMapping("/getVideoCommentCount/{id}")
    public Long getVideoCommentCount(@PathVariable Long id){
        return videoCommentService.getVideoCommentCount(id);
    }


}
