package com.zhou.comment.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhou.model.comment.VideoComment;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface VideoCommentMapper extends BaseMapper<VideoComment> {
}
