package com.zhou.chat.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhou.model.chat.pojos.UserMessage;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMessageMapper extends BaseMapper<UserMessage> {
}
