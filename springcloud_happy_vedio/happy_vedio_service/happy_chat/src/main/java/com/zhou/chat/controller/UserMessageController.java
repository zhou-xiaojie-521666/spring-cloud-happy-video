package com.zhou.chat.controller;

import com.zhou.chat.enums.MsgTypeEnum;
import com.zhou.chat.message.ChatMsgMessage;
import com.zhou.chat.service.UserMessageService;
import com.zhou.model.chat.pojos.UserMessage;
import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.model.user.pojos.UserInfo;
import com.zhou.utils.thread.AppThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/chat")
public class UserMessageController {

    @Autowired
    private UserMessageService userMessageService;


    @RequestMapping("/getChatHistory/{fromId}/{toId}")
    public ResponseResult send(@PathVariable("fromId") Long fromId, @PathVariable("toId") Long toId) {
        List<UserMessage> chatHistory = userMessageService.getChatHistory(fromId, toId);
        return ResponseResult.okResult(chatHistory);
    }

    @RequestMapping("/getLinkMan/{userId}")
    public ResponseResult getLinkMan(@PathVariable Long userId){
        List<UserInfo> userInfoList=userMessageService.getLinkMan(userId);
        return ResponseResult.okResult(userInfoList);
    }

    @RequestMapping("/getLinkManByToken")
    public ResponseResult getLinkManByToken(){
        UserInfo user = AppThreadLocalUtil.getUser();
        List<UserInfo> userInfoList=userMessageService.getLinkMan(user.getId());
        return ResponseResult.okResult(userInfoList);
    }

    @RequestMapping("/insertMessage")
    public ResponseResult insertMessage(UserMessage userMessage){
        userMessageService.insertMessage(userMessage);
        return ResponseResult.okResult("发送信息成功");
    }

    @RequestMapping("/getNotice")
    public ResponseResult getNotice(Integer currentIndex){
        List<UserMessage> notice = userMessageService.getNotice(currentIndex);
        return ResponseResult.okResult(notice);
    }


}
