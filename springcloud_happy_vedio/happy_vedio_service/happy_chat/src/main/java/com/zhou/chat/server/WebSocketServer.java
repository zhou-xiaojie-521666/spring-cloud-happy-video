package com.zhou.chat.server;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
//import com.zhou.chat.context.WebSocketContext;
import com.zhou.chat.enums.MsgTypeEnum;
import com.zhou.chat.mapper.UserMessageMapper;
import com.zhou.chat.message.ChatCountMessage;
import com.zhou.chat.message.ChatMsgMessage;
import com.zhou.chat.message.Message;
import com.zhou.chat.service.UserMessageService;
import com.zhou.model.chat.WebSocketMessageEntity;
import com.zhou.model.chat.enums.MessageTypeEnums;
import com.zhou.model.chat.pojos.UserMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * 配置接入点
 * ServerEndpoint 注解是一个类层次的注解，它的功能主要是将目前的类定义成一个websocket服务器端,
 * 注解的值将被用于监听用户连接的终端访问URL地址,客户端可以通过这个URL来连接到WebSocket服务器端
 */
@Component
@ServerEndpoint("/chat/{fromId}/{toId}")
@Service
@Slf4j
public class WebSocketServer {


    private static UserMessageService userMessageService;

    @Autowired
    public void setLocationMapper(UserMessageService userMessageService){
        WebSocketServer.userMessageService=userMessageService;
    }


    private static UserMessageMapper userMessageMapper;

    @Autowired
    public void setLocationMapper(UserMessageMapper userMessageMapper){
        WebSocketServer.userMessageMapper=userMessageMapper;
    }

    /**
     * Session 与用户的映射
     */
    private static final Map<Session, Long> SESSION_USER_MAP = new ConcurrentHashMap<>();

    /**
     * 用户与 Session 的映射
     */
    private static final Map<Long, Session> USER_SESSION_MAP = new ConcurrentHashMap<>();

    /**
     * 离线消息
     */
    private ConcurrentHashMap<String, List<WebSocketMessageEntity>> offlineMessage = new ConcurrentHashMap<>();

    /**
     * 新建list集合存储数据
     */
    private static List<UserMessage> messageList = new ArrayList<>();


    /**
     * 每当list的长度达到固定值时，向数据库存储一次
     */
    private static final Integer LIST_SIZE = 5;
    /**
     * 用来存放每个客户端对应的MyWebSocket对象。
     **/
    private static CopyOnWriteArraySet<WebSocketServer> webSocketSet = new CopyOnWriteArraySet<>();
    /**
     * 与某个客户端的连接会话，需要通过它来给客户端发送数据
     **/
    private Session session;
    /**
     * 用来记录userId和该session进行绑定
     **/
    private static Map<Long, Session> map = new HashMap<Long, Session>();


    /**
     * 连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("fromId") Long fromId, @PathParam("toId") Long toId) {
        if (toId == null) {
            log.info("暂无聊天");
            return;
        }
        log.info("{}正在和{}聊天,session:{}", fromId, toId, session);

        addSessionMapping(fromId, session);
        log.info("{} 开启连接，Session ID: {}", fromId, session.getId());
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(Session session, @PathParam("fromId") Long fromId, @PathParam("toId") Long toId) {
        removeSessionMapping(session); // 根据Session移除映射
//        if(messageList.size()>0){
//            userMessageService.insertMessageList(messageList);
//            //清除集合
//            messageList.clear();
//        }
        log.info("连接关闭, Session ID: {}, 用户ID: {}", session.getId(), fromId);
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param message 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String message, Session session, @PathParam("fromId") Long fromId, @PathParam("toId") Long toId) {
        log.info("接收到来自用户ID: {} 的消息: {},发送给", fromId, message, toId);
        //将信息保存到数据库
        ChatMsgMessage msg = JSON.parseObject(message, ChatMsgMessage.class);
        UserMessage userMessage = new UserMessage();
        userMessage.setFromId(fromId);
        userMessage.setToId(toId);
        userMessage.setMessage(msg.getMsg());
        userMessage.setCreateTime(new Date());
        userMessage.setType(MessageTypeEnums.COMMON.getType());
        userMessageMapper.insert(userMessage);
//        if(messageList.size()==LIST_SIZE){
//            userMessageService.insertMessageList(messageList);
//            //清除集合
//            messageList.clear();
//        }
        // 发送消息给指定的接收者
        sendMessageToUser(toId, message);
    }

    /**
     * 监听错误
     *
     * @param session session
     * @param error   错误
     */
    @OnError
    public void onError(Session session, Throwable error) {
        log.error("SessionId:{},出现异常：{}", session.getId(), error.getMessage());
        error.printStackTrace();
    }


    /**
     * 添加 Session 在这个方法中，会绑定用户和 Session 之间的映射
     */
    private static void addSessionMapping(Long userId, Session session) {
        SESSION_USER_MAP.put(session, userId);
        USER_SESSION_MAP.put(userId, session);
    }

    // 发送消息给指定用户，其中处理了查找正确Session的逻辑
    private void sendMessageToUser(Long userId, String message) {
        Session session = USER_SESSION_MAP.get(userId);
        if (session != null) {
            sendTextMessage(session, message);
        } else {
            // 用户不在线或者Session未找到，处理离线消息等情况
            log.warn("用户 {} 不在线，消息发送失败。", userId);
            //发送一条消息，通知接收者
        }
    }


    private static void removeSessionMapping(Session session) {
        Long userId = SESSION_USER_MAP.remove(session);
        if (userId != null) {
            USER_SESSION_MAP.remove(userId);
        }
    }


    /**
     * 真正发送消息
     *
     * @param session     Session
     * @param messageText 消息
     */
    private static void sendTextMessage(Session session, String messageText) {
        if (session == null) {
            log.error("===> session 为 null");
            return;
        }
        RemoteEndpoint.Basic basic = session.getBasicRemote();
        if (basic == null) {
            log.error("===> session.basic 为 null");
            return;
        }
        try {
            basic.sendText(messageText);
            log.info("===> 发送成功{}", messageText);
        } catch (IOException e) {
            log.error("===> session: {} 发送消息: {} 发生异常", session, messageText, e);
        }
    }


}
