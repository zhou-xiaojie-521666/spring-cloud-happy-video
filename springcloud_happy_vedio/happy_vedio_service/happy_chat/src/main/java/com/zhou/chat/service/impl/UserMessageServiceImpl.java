package com.zhou.chat.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhou.chat.feign.UserClient;
import com.zhou.chat.mapper.UserMessageMapper;
import com.zhou.chat.service.UserMessageService;
import com.zhou.common.exception.BusinessException;
import com.zhou.model.chat.enums.MessageTypeEnums;
import com.zhou.model.chat.pojos.UserMessage;
import com.zhou.model.user.pojos.UserInfo;
import com.zhou.utils.thread.AppThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserMessageServiceImpl extends ServiceImpl<UserMessageMapper, UserMessage> implements UserMessageService {

    @Autowired
    private UserClient userClient;

    @Override
    public List<UserMessage> getChatHistory(Long fromId, Long toId) {
        LambdaQueryWrapper<UserMessage> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(UserMessage::getType, MessageTypeEnums.COMMON.getType(), MessageTypeEnums.SHARE.getType());
        queryWrapper.and(wrapper -> wrapper
                        .eq(UserMessage::getFromId, fromId)
                        .eq(UserMessage::getToId, toId))
                .or(wrapper -> wrapper
                        .eq(UserMessage::getFromId, toId)
                        .eq(UserMessage::getToId, fromId));

        List<UserMessage> list = list(queryWrapper);
        list=list.stream().map((item)->{
            Long toId1 = item.getToId();
            Long fromId1 = item.getFromId();
            UserInfo toUserInfo = userClient.getUserInfoById(toId1);
            UserInfo fromUserInfo = userClient.getUserInfoById(fromId1);
            item.setFromUserAvatar(toUserInfo.getAvatar());
            item.setToUserAvatar(fromUserInfo.getAvatar());
            if (fromId1 == fromId){
                item.setFrom("other");
            }else {
                item.setFrom("me");
            }
            return item;
        }).collect(Collectors.toList());
        return list;
    }

    @Override
    public void insertMessageList(List<UserMessage> userMessage) {
        userMessage.forEach((item)->{
            save(item);
        });


    }

    @Override
    public List<UserInfo> getLinkMan(Long userId) {
        // 查询接收人是自己或者发送人是自己的消息。
        LambdaQueryWrapper<UserMessage> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserMessage::getToId, userId).or().eq(UserMessage::getFromId, userId);
        queryWrapper.orderByDesc(UserMessage::getCreateTime);
        List<UserMessage> userMessageList = list(queryWrapper);
        // 使用Set来存储已处理过的userId，确保唯一性
        Set<Long> processedUserIds = new HashSet<>();
        List<UserInfo> userInfoList = new ArrayList<>();

        userMessageList.forEach(item -> {
            Long toId = item.getToId();
            Long fromId = item.getFromId();

            // 确定非当前userId的唯一userId
            Long otherUserId = userId.equals(toId) ? fromId : toId;

            // 如果是新的userId，获取该用户信息并加入列表
            if (processedUserIds.add(otherUserId)) { // add()返回true表示是新元素
                UserInfo userInfoById = userClient.getUserInfoById(otherUserId);
                if (userInfoById != null) {
                    userInfoList.add(userInfoById);
                }
            }
        });

        return userInfoList; // 返回已填充的联系人列表
    }

    @Override
    public void insertMessage(UserMessage userMessage) {
        System.out.println(userMessage);
        save(userMessage);
    }

    @Override
    public List<UserMessage> getNotice(Integer currentIndex) {
        UserInfo user = AppThreadLocalUtil.getUser();
        if (user==null){
            throw new BusinessException("权限错误");
        }
        int pageSize = 10;
        int start = currentIndex * pageSize;
        LambdaQueryWrapper<UserMessage> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserMessage::getType, MessageTypeEnums.INFORM.getType())
                .eq(UserMessage::getToId,user.getId())
                .orderByDesc(UserMessage::getCreateTime)
                .last("limit " + start + ", " + pageSize);

        List<UserMessage> list = list(queryWrapper);
        list=list.stream().map((item)->{
            Long toId1 = item.getToId();
            Long fromId1 = item.getFromId();
            UserInfo toUserInfo = userClient.getUserInfoById(toId1);
            UserInfo fromUserInfo = userClient.getUserInfoById(fromId1);
            item.setFromUserAvatar(toUserInfo.getAvatar());
            item.setToUserAvatar(fromUserInfo.getAvatar());
            item.setToUserNickName(toUserInfo.getNickName());
            item.setFromUserNickName(fromUserInfo.getNickName());
            return item;
        }).collect(Collectors.toList());

        return list;
    }
}
