package com.zhou.chat.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhou.model.chat.pojos.UserMessage;
import com.zhou.model.user.pojos.UserInfo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserMessageService extends IService<UserMessage> {
    List<UserMessage> getChatHistory(Long fromId,Long toId);

    void insertMessageList(List<UserMessage> userMessage);

    List<UserInfo> getLinkMan(Long userId);

    void insertMessage(UserMessage userMessage);

    List<UserMessage> getNotice(Integer currentIndex);

}
