package com.zhou.chat.message;

import com.zhou.chat.enums.MsgTypeEnum;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 发送给所有人的群聊消息的 Message
 *
 */
@Data
@Accessors(chain = true)
public class ChatMsgMessage implements Message {

    public static final String TYPE = MsgTypeEnum.CHAT_MSG.getCode();

    /**
     * 消息编号
     */
    private String msgId;
    /**
     * 内容
     */
    private String msg;

}
