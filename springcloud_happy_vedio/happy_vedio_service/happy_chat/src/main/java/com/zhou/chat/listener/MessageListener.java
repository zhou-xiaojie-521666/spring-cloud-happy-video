package com.zhou.chat.listener;

import com.zhou.chat.service.UserMessageService;
import com.zhou.model.MqContants;
import com.zhou.model.chat.pojos.UserMessage;
import com.zhou.model.vedio.dto.SearchArticleVo;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class MessageListener {

    @Autowired
    private UserMessageService userMessageService;

    /**
     * 监听新增和修改的
     */
    @RabbitListener(queues = MqContants.MESSAGE_INSERT_QUEUE)
    public void listenHotelInsertOrUpdate(UserMessage userMessage) throws IOException {
        userMessageService.insertMessage(userMessage);
    }


    /**
     * 监听删除
     */
//    @RabbitListener(queues = MqContants.VIDEO_DELETE_QUEUE)
//    public void listenHotelDelete(Long id){
//        userMessageService.deleteElasticSearch(id);
//    }
}
