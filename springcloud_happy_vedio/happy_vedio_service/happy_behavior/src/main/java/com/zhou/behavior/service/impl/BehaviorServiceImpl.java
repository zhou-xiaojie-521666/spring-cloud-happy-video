package com.zhou.behavior.service.impl;

import com.zhou.behavior.feign.VideoClient;
import com.zhou.behavior.service.BehaviorService;
import com.zhou.behavior.utils.RedisUtil;
import com.zhou.model.MqContants;
import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.model.user.pojos.UserInfo;
import com.zhou.model.vedio.pojos.Video;
import com.zhou.utils.thread.AppThreadLocalUtil;
import org.junit.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class BehaviorServiceImpl implements BehaviorService {

    @Autowired
    private VideoClient videoClient;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private RabbitTemplate rabbitTemplate;


    @Override
    public Object getList() {
        Object allVideo = redisUtil.get("allVideo");
        return allVideo;
    }

    @Override
    public boolean likeForVideo(Long videoId) {
        UserInfo user = AppThreadLocalUtil.getUser();
        Long userId = user.getId();
        //添加到redis
        boolean b = redisUtil.addLike(videoId, userId);
//        if (b){
//
//            rabbitTemplate.convertAndSend(MqContants.Behavior_EXCHANGE,MqContants.Behavior_INSERT_QUEUE,);
//        }
        return b;
    }

    @Override
    public boolean hasLiked(Long videoId) {
        UserInfo user = AppThreadLocalUtil.getUser();
        boolean b = redisUtil.hasLiked(videoId, user.getId());
        return b;
    }

    @Override
    public boolean delLiked(Long videoId) {
        UserInfo user = AppThreadLocalUtil.getUser();
        boolean b = redisUtil.removeLike(videoId, user.getId());
        return b;
    }

    @Override
    public long getLikesCount(Long videoId) {
        long likesCount = redisUtil.getLikesCount(videoId);
        return likesCount;
    }

    @Override
    public List<Video> getLikedVideos(Integer id) {
//        UserInfo user = AppThreadLocalUtil.getUser();
        Set<Object> likedVideos = redisUtil.getLikedVideos(Long.valueOf(id));
        List<Long> videoIds = likedVideos.stream()
                .map(Object::toString)
                .map(Long::parseLong)
                .collect(Collectors.toList());
        List<Video> videoList=new ArrayList<>();
        videoIds.stream().map((item)->{
            Video video = videoClient.clientVideoById(item);
            videoList.add(video);
            return item;
        }).collect(Collectors.toSet());
        return videoList;
    }
}
