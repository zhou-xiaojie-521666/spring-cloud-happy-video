package com.zhou.behavior.feign;


import com.zhou.model.vedio.pojos.Video;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("happy-vedio")
public interface VideoClient {
    @RequestMapping("/api/vedio/clientVideoById/{id}")
    Video clientVideoById(@PathVariable Long id);
}
