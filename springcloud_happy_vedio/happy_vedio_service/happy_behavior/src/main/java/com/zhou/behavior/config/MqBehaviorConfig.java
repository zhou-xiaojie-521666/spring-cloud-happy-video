package com.zhou.behavior.config;

import com.zhou.model.MqContants;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MqBehaviorConfig {
    @Bean
    public TopicExchange BehaviorTopicExchange(){
        return new TopicExchange(MqContants.Behavior_EXCHANGE,true,false);
    }

    @Bean
    public Queue BehaviorInsertQueue(){
        return new Queue(MqContants.Behavior_INSERT_QUEUE,true);
    }

    @Bean
    public Queue BehaviorDeleteQueue(){
        return new Queue(MqContants.Behavior_DELETE_QUEUE,true);
    }

    @Bean
    public Binding BehaviorInsertQueueBinding(){
        return BindingBuilder.bind(BehaviorInsertQueue()).to(BehaviorTopicExchange()).with(MqContants.Behavior_INSERT_KEY);
    }

    @Bean
    public Binding BehaviorDeleteQueueBinding(){
        return BindingBuilder.bind(BehaviorDeleteQueue()).to(BehaviorTopicExchange()).with(MqContants.Behavior_DELETE_KEY);
    }
}
