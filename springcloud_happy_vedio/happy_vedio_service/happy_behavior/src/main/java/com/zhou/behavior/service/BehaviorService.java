package com.zhou.behavior.service;

import com.zhou.model.vedio.pojos.Video;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BehaviorService {

    Object getList();

    boolean likeForVideo(Long videoId);

    boolean hasLiked(Long videoId);

    boolean delLiked(Long videoId);

    long getLikesCount(Long videoId);

    List<Video> getLikedVideos(Integer id);

}
