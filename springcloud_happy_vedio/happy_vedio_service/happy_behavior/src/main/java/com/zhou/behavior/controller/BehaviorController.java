package com.zhou.behavior.controller;

import com.zhou.behavior.service.BehaviorService;
import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.model.vedio.pojos.Video;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;


@RestController
@RequestMapping("/api/behavior")
@Api(value = "用户行为", description = "用户行为API")
public class BehaviorController {

    @Autowired
    private BehaviorService behaviorService;

    @RequestMapping("/test")
    public Object test(){
        Object list = behaviorService.getList();
        return list;
    }

    @RequestMapping("/likeForVideo")
    public ResponseResult likeForVideo(Long videoId){
        boolean b = behaviorService.likeForVideo(videoId);
        return b?ResponseResult.okResult(200):ResponseResult.okResult(201);
    }


    @RequestMapping("/hasLiked")
    public ResponseResult hasLiked(Long videoId){
        boolean b = behaviorService.hasLiked(videoId);
        //点赞200 未点赞201
        return b?ResponseResult.okResult(200):ResponseResult.okResult(201);
    }


    @RequestMapping("/delLiked")
    public ResponseResult delLiked(Long videoId){
        boolean b = behaviorService.delLiked(videoId);
        //成功200 失败201
        return b?ResponseResult.okResult(200):ResponseResult.okResult(201);
    }

    @RequestMapping("/getLikesCount")
    public ResponseResult getLikesCount(Long videoId){
        long likesCount = behaviorService.getLikesCount(videoId);
        return ResponseResult.okResult(likesCount);
    }

    @RequestMapping("/getLikedVideos/{id}")
    public ResponseResult getLikedVideos(@PathVariable Integer id){
        List<Video> likedVideos = behaviorService.getLikedVideos(id);
        return ResponseResult.okResult(likedVideos);
    }


}
