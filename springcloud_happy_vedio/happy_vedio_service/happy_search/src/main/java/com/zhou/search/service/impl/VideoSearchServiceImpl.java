package com.zhou.search.service.impl;

import com.alibaba.fastjson.JSON;
import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.model.common.enums.AppHttpCodeEnum;
import com.zhou.model.search.dtos.UserSearchDto;
import com.zhou.model.user.pojos.UserInfo;
import com.zhou.search.service.AssociateWordsService;
import com.zhou.search.service.UserSearchService;
import com.zhou.search.service.VideoSearchService;
import com.zhou.utils.thread.AppThreadLocalUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class VideoSearchServiceImpl implements VideoSearchService {


    @Autowired
    private RestHighLevelClient restHighLevelClient;

    @Autowired
    private UserSearchService apUserSearchService;

    @Autowired
    private AssociateWordsService associateWordsService;

    @Override
    public ResponseResult search(UserSearchDto dto) throws IOException {
        //检查参数
        if (dto==null&& StringUtils.isBlank(dto.getSearchWords())){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        Integer board=null;
        if (dto.getBoard()=="user"){
            //TODO 查询用户
            board=2;
            return ResponseResult.okResult(null);
        }else if (dto.getBoard()=="video"){
            board=1;
        }

        //异步调用保存搜索记录
        UserInfo user = AppThreadLocalUtil.getUser();
        if (user!=null){
            apUserSearchService.insert(dto.getSearchWords(), user.getId());
        }

        //TODO 调用保存搜索联想词
        associateWordsService.insertAssociate(dto.getSearchWords());

        //设置查询条件
        SearchRequest searchRequest=new SearchRequest("app_info_video");
        SearchSourceBuilder searchSourceBuilder=new SearchSourceBuilder();
        //boolean查询构建器
        BoolQueryBuilder boolQueryBuilder=new BoolQueryBuilder();
        //关键字分词之后的查询
        QueryStringQueryBuilder queryStringQueryBuilder = QueryBuilders.queryStringQuery(dto.getSearchWords()).field("title").field("description").defaultOperator(Operator.OR);
        boolQueryBuilder.must(queryStringQueryBuilder);
        //查询小于mindata的数据
        RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery("gmtCreated");
        boolQueryBuilder.filter(rangeQueryBuilder);

        // 只查询isDeleted字段为0
        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("isDeleted", 0);
        boolQueryBuilder.filter(termQueryBuilder);
        //分页
        searchSourceBuilder.from(0);
        //按照发布时间
        if (dto.getScreen() == "newPost"){
            searchSourceBuilder.sort("gmtCreated", SortOrder.DESC);
        }
        if (dto.getScreen()=="composite"){
            //TODO 根据热度排序
        }

        //设置高亮
        HighlightBuilder highlightBuilder=new HighlightBuilder();
        highlightBuilder.field("title");
        highlightBuilder.preTags("<font style='color: red; font-size: inherit;'>");
        highlightBuilder.postTags("</font>");
        searchSourceBuilder.highlighter(highlightBuilder);

        searchSourceBuilder.query(boolQueryBuilder);
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);

        //结果封装返回
        List<Map> list = new ArrayList<>();

        SearchHit[] hits = searchResponse.getHits().getHits();
        for (SearchHit hit : hits) {
            String json = hit.getSourceAsString();
            Map map = JSON.parseObject(json, Map.class);
            //处理高亮
            if(hit.getHighlightFields() != null && hit.getHighlightFields().size() > 0){
                Text[] titles = hit.getHighlightFields().get("title").getFragments();
                String title = StringUtils.join(titles);
                //高亮标题
                map.put("h_title",title);
            }else {
                //原始标题
                map.put("h_title",map.get("title"));
            }
            list.add(map);
        }
        return ResponseResult.okResult(list);
    }
}
