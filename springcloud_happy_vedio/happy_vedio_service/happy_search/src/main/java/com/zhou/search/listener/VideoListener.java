package com.zhou.search.listener;

import com.zhou.model.MqContants;
import com.zhou.model.vedio.dto.SearchArticleVo;
import com.zhou.search.service.UserSearchService;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class VideoListener {

    @Autowired
    private UserSearchService userSearchService;

    /**
     * 监听新增
     */
    @RabbitListener(queues = MqContants.VIDEO_INSERT_QUEUE)
    public void listenHotelInsertOrUpdate(SearchArticleVo searchArticleVo) throws IOException {
        userSearchService.insertElasticSearch(searchArticleVo);
    }

    /**
     * 监听修改
     */
    @RabbitListener(queues = MqContants.VIDEO_UPDATE_QUEUE)
    public void listenUpdate(SearchArticleVo searchArticleVo) throws IOException {
        userSearchService.updateElastic(searchArticleVo);
    }



    /**
     * 监听删除
     */
    @RabbitListener(queues = MqContants.VIDEO_DELETE_QUEUE)
    public void listenHotelDelete(Long id) throws IOException {
        userSearchService.deleteElasticSearch(id);
    }
}
