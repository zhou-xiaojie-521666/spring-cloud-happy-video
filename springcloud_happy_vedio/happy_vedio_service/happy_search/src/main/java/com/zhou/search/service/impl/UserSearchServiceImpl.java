package com.zhou.search.service.impl;

import com.alibaba.fastjson.JSON;
import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.model.common.enums.AppHttpCodeEnum;
import com.zhou.model.user.pojos.UserInfo;
import com.zhou.model.vedio.dto.SearchArticleVo;
import com.zhou.search.pojo.UserSearch;
import com.zhou.search.pojo.HistorySearchDto;
import com.zhou.search.service.UserSearchService;
import com.zhou.utils.thread.AppThreadLocalUtil;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.rest.RestStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class UserSearchServiceImpl implements UserSearchService {

    @Autowired
    private MongoTemplate mongoTemplate;


    @Autowired
    private RestHighLevelClient restHighLevelClient;



    @Override
    public ResponseResult delUserSearch(HistorySearchDto dto) {
        //1.检查参数
        if (dto.getId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //2.判断是否登录
        UserInfo user = AppThreadLocalUtil.getUser();
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }

        //3.删除
        mongoTemplate.remove(Query.query(Criteria.where("userId").is(user.getId()).and("id").is(dto.getId())), UserSearch.class);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    @Override
    public void insertElasticSearch(SearchArticleVo searchArticleVo) throws IOException {
        //2.导入到es索引
        BulkRequest bulkRequest = new BulkRequest("app_info_video");

        IndexRequest indexRequest = new IndexRequest().id(searchArticleVo.getId().toString())
                .source(JSON.toJSONString(searchArticleVo), XContentType.JSON);
        //添加数据
        bulkRequest.add(indexRequest);
        // 执行BulkRequest
        BulkResponse bulkResponse = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);

        // 处理可能的错误
        if (bulkResponse.hasFailures()) {
            // 记录错误或根据您的需求处理它
            System.err.println("批量请求执行出现错误： " + bulkResponse.buildFailureMessage());
            // 你可能想要抛出异常或执行其它错误处理。
            throw new IOException("执行批量请求出错： " + bulkResponse.buildFailureMessage());
        }
    }

    @Override
    public void deleteElasticSearch(Long id) throws IOException {
        System.out.println(id);
        try {
            DeleteRequest request=new DeleteRequest("app_info_video",id.toString());
            restHighLevelClient.delete(request,RequestOptions.DEFAULT);
            System.out.println("删除成功");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
//        // 创建一个 DeleteRequest，指定索引名和文档ID
//        RestHighLevelClient client=null;
//        DeleteRequest deleteRequest = new DeleteRequest("app_info_video", id.toString());
//        // 执行删除操作
//        DeleteResponse deleteResponse = client.delete(deleteRequest, RequestOptions.DEFAULT);
//
//        // 检查操作的结果状态
//        if (deleteResponse.status() == RestStatus.OK) {
//            System.out.println("Document deleted successfully.");
//        } else if (deleteResponse.status() == RestStatus.NOT_FOUND) {
//            System.out.println("Document not found.");
//        } else {
//            System.out.println("Failed to delete document. Status: " + deleteResponse.status());
//        }
    }

    @Override
    public void updateElastic(SearchArticleVo searchArticleVo) throws IOException {
        // 创建 BulkRequest 对象，用于提交批量操作请求
        BulkRequest bulkRequest = new BulkRequest();
        // 创建 UpdateRequest 对象，并设置索引名和文档ID
        UpdateRequest updateRequest = new UpdateRequest("app_info_video", searchArticleVo.getId().toString());

        // 使用 searchArticleVo 对象的 JSON 字符串作为更新内容
        updateRequest.doc(JSON.toJSONString(searchArticleVo), XContentType.JSON);

        // 将 UpdateRequest 添加到 BulkRequest 中
        bulkRequest.add(updateRequest);

        // 执行批量操作
        BulkResponse bulkResponse = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);

        // 检查操作结果
        if (bulkResponse.hasFailures()) {
            // 如果存在失败的操作，处理这些失败情况
            System.out.println("Bulk update failed: " + bulkResponse.buildFailureMessage());
        } else {
            System.out.println("Bulk update completed successfully");
        }
    }


    @Override
    public ResponseResult findUserSearch() {
        //获取当前用户
        UserInfo user = AppThreadLocalUtil.getUser();
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }

        //根据用户查询数据，按照时间倒序
        List<UserSearch> apUserSearches = mongoTemplate.find(Query.query(
                        Criteria.where("userId").is(user.getId())).
                with(Sort.by(Sort.Direction.DESC, "createdTime")), UserSearch.class);
        return ResponseResult.okResult(apUserSearches);
    }

    @Override
    @Async
    public void insert(String keyword, Long userId) {
        //1.查询当前用户的搜索关键词
        Query query = new Query(Criteria.where("userId").is(userId).and("keyword").is(keyword));
        UserSearch apUserSearch = mongoTemplate.findOne(query, UserSearch.class);

        if (keyword == null || keyword == " ") {
            return;
        }
        //2.存在 更新创建时间
        if (apUserSearch != null) {
            apUserSearch.setCreatedTime(new Date());
            mongoTemplate.save(apUserSearch);
            return;
        }
        //3.不存在，判断当前历史记录总数量是否超过10
        apUserSearch = new UserSearch();
        apUserSearch.setUserId(userId);
        apUserSearch.setKeyword(keyword);
        apUserSearch.setCreatedTime(new Date());
        Query query1 = Query.query(Criteria.where("userId").is(userId));
        query1.with(Sort.by(Sort.Direction.DESC, "createdTime"));
        List<UserSearch> apUserSearchList = mongoTemplate.find(query1, UserSearch.class);

        if (apUserSearchList == null || apUserSearchList.size() < 10) {
            mongoTemplate.save(apUserSearch);
        } else {
            UserSearch lastUserSearch = apUserSearchList.get(apUserSearchList.size() - 1);
            mongoTemplate.findAndReplace(Query.query(Criteria.where("id").is(lastUserSearch.getId())), apUserSearch);
        }
    }
}
















