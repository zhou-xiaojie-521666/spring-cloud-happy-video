package com.zhou.search.service;

import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.model.vedio.dto.SearchArticleVo;
import com.zhou.search.pojo.HistorySearchDto;
import org.elasticsearch.client.RestHighLevelClient;

import java.io.IOException;

public interface UserSearchService {
    /**
     * 保存用户搜索记录
     * @param keyword
     * @param userId
     */
    public void insert(String keyword,Long userId);

    /**
     查询搜索历史
     @return
     */
    ResponseResult findUserSearch();


    /**
     删除搜索历史
     @param historySearchDto
     @return
     */
    ResponseResult delUserSearch(HistorySearchDto historySearchDto);

    /**
     * 同步数据到elasticSearch
     */
    void insertElasticSearch(SearchArticleVo searchArticleVo) throws IOException;

    void deleteElasticSearch(Long id) throws IOException;

    void updateElastic(SearchArticleVo searchArticleVo) throws IOException;
}
