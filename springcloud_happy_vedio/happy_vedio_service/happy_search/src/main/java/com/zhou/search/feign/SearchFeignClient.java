package com.zhou.search.feign;

import com.zhou.apis.schedule.ISearchFeignClient;
import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.model.vedio.dto.SearchArticleVo;
import com.zhou.search.service.UserSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;


@RestController
public class SearchFeignClient implements ISearchFeignClient {

    @Autowired
    private UserSearchService userSearchService;

    @PostMapping("/api/search/insertElastic")
    @Override
    public ResponseResult insertElastic(@RequestBody SearchArticleVo searchArticleVo) throws IOException {

        userSearchService.insertElasticSearch(searchArticleVo);
        return ResponseResult.okResult(null);
    }

    @RequestMapping("/api/search/updateElastic")
    public ResponseResult updateElastic(@RequestBody SearchArticleVo searchArticleVo) throws IOException{
        userSearchService.updateElastic(searchArticleVo);
        return ResponseResult.okResult(null);
    }
}
