package com.zhou.search.controller.v1;

import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.model.search.dtos.UserSearchDto;
import com.zhou.search.service.VideoSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/api/video/search")
public class VideoSearchController {

    @Autowired
    private VideoSearchService articleSearchService;

    @PostMapping("/search")
    public ResponseResult search(@RequestBody UserSearchDto dto) throws IOException {
        return articleSearchService.search(dto);
    }
}
