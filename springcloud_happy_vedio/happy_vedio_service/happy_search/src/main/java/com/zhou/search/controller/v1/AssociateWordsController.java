package com.zhou.search.controller.v1;

import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.model.search.dtos.CorrelationSearchDto;
import com.zhou.model.search.dtos.UserSearchDto;
import com.zhou.search.service.AssociateWordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/associate")
public class AssociateWordsController {

    @Autowired
    private AssociateWordsService apAssociateWordsService;

    @PostMapping("/search")
    public ResponseResult findAssociate(@RequestBody UserSearchDto userSearchDto) {
        return apAssociateWordsService.findAssociate(userSearchDto);
    }

    @RequestMapping("/guessWant")
    public ResponseResult guessWant(){
        return apAssociateWordsService.guessWant();
    }

    @RequestMapping("/correlationSearch")
    public ResponseResult correlationSearch(@RequestBody CorrelationSearchDto userSearchDto){
        return apAssociateWordsService.correlationSearch(userSearchDto.getSearchWords());
    }
}