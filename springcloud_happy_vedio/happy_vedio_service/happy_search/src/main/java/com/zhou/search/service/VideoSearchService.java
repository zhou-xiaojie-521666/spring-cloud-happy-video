package com.zhou.search.service;

import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.model.search.dtos.UserSearchDto;

import java.io.IOException;

public interface VideoSearchService {

    /**
     * es文章搜索
     * @param dto
     * @return
     */
    public ResponseResult search(UserSearchDto dto) throws IOException;
}
