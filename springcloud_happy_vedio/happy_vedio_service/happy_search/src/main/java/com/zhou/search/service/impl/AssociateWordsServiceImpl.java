package com.zhou.search.service.impl;

import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.model.common.enums.AppHttpCodeEnum;
import com.zhou.model.search.dtos.UserSearchDto;
import com.zhou.search.pojo.ApAssociateWords;
import com.zhou.search.service.AssociateWordsService;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import javax.xml.crypto.Data;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @Description:
 * @Version: V1.0
 */
@Service
public class AssociateWordsServiceImpl implements AssociateWordsService {

    @Autowired
    MongoTemplate mongoTemplate;

    /**
     * 联想词
     * @param userSearchDto
     * @return
     */
    @Override
    public ResponseResult findAssociate(UserSearchDto userSearchDto) {
        //1 参数检查
        if(userSearchDto == null || StringUtils.isBlank(userSearchDto.getSearchWords())){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //3 执行查询 模糊查询
        String searchWords = Pattern.quote(userSearchDto.getSearchWords().trim());
        Query query = Query.query(Criteria.where("associateWords").regex(".*?" + searchWords + ".*"));
        query.limit(10);
        List<ApAssociateWords> wordsList = mongoTemplate.find(query, ApAssociateWords.class);
        return ResponseResult.okResult(wordsList);
    }

    @Override
    public void insertAssociate(String searchWords) {
        // 确保searchWords有效
        if(StringUtils.isBlank(searchWords)) {
            throw new IllegalArgumentException("searchWords cannot be null or empty");
        }

        // 尝试查找已存在的记录
        Query query = Query.query(Criteria.where("associateWords").is(searchWords));
        ApAssociateWords existingWord = mongoTemplate.findOne(query, ApAssociateWords.class);

        // 如果已存在，增加计数
        if (existingWord != null) {
            Update update = new Update().inc("number", 1); // 将number字段的值加一
            mongoTemplate.findAndModify(query, update, ApAssociateWords.class);
        } else {
            // 如果不存在，创建新记录
            ApAssociateWords newAssociateWord = new ApAssociateWords(null, searchWords,0, new Date());
            mongoTemplate.save(newAssociateWord);
        }
    }

    @Override
    public ResponseResult guessWant() {
        // 创建查询对象，并指定排序方式为number字段降序
        Query query = new Query().with(Sort.by(Sort.Direction.DESC, "number"));
        // 限制结果数量为6
        query.limit(6);

        // 执行查询
        List<ApAssociateWords> results = mongoTemplate.find(query, ApAssociateWords.class);

        // 将结果包装在ResponseResult中并返回
        // 假设ResponseResult有一个接受List<ApAssociateWords>的构造函数或者setter方法
        return ResponseResult.okResult(results);
    }

    @Override
    public ResponseResult correlationSearch(String searchWords) {
        if(searchWords == null || StringUtils.isBlank(searchWords)){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //2 执行查询 模糊查询,并且根据number排序
        String searchWordsNew = Pattern.quote(searchWords.trim());
        Query query = Query.query(Criteria.where("associateWords").regex(".*?" + searchWordsNew + ".*"));
        query.with(Sort.by(Sort.Direction.DESC, "number"));
        query.limit(10);
        List<ApAssociateWords> wordsList = mongoTemplate.find(query, ApAssociateWords.class);
        return ResponseResult.okResult(wordsList);
    }
}