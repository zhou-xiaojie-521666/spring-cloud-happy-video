package com.zhou.search.service;

import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.model.search.dtos.UserSearchDto;

/**
 * 联想词表 服务类
 */
public interface AssociateWordsService {

    /**
     联想词
     @param userSearchDto
     @return
     */
    ResponseResult findAssociate(UserSearchDto userSearchDto);

    void insertAssociate(String searchWords);

    ResponseResult guessWant();

    ResponseResult correlationSearch(String searchWords);
}