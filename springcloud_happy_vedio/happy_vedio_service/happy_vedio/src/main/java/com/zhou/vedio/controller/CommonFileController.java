package com.zhou.vedio.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zhou.model.Constants;
import com.zhou.model.vedio.pojos.Video;
import com.zhou.utils.StringTools;
import com.zhou.vedio.config.AppConfig;
import com.zhou.vedio.service.VideoService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;


public class CommonFileController {

    @Resource
    private AppConfig appConfig;

    @Resource
    private VideoService videoService;

    private static final Logger logger = LoggerFactory.getLogger(CommonFileController.class);


    protected void readFile(HttpServletResponse response, String filePath) {
        if (!StringTools.pathIsOk(filePath)) {
            return;
        }
        OutputStream out = null;
        FileInputStream in = null;
        try {
            File file = new File(filePath);
            if (!file.exists()) {
                return;
            }
            in = new FileInputStream(file);
            byte[] byteData = new byte[1024];
            out = response.getOutputStream();
            int len = 0;
            while ((len = in.read(byteData)) != -1) {
                out.write(byteData, 0, len);
            }
            out.flush();
        } catch (Exception e) {
            logger.error("读取文件异常", e);
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    logger.error("IO异常", e);
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    logger.error("IO异常", e);
                }
            }
        }
    }

    public void getImage(HttpServletResponse response, String imageFolder, String imageName) {
        if (StringTools.isEmpty(imageFolder) || StringUtils.isBlank(imageName)) {
            return;
        }
        String imageSuffix = StringTools.getFileSuffix(imageName);
        String filePath = appConfig.getProjectFolder() + Constants.FILE_FOLDER_FILE + imageFolder + "/" + imageName;
        imageSuffix = imageSuffix.replace(".", "");
        String contentType = "image/" + imageSuffix;
        response.setContentType(contentType);
        response.setHeader("Cache-Control", "max-age=2592000");
        readFile(response, filePath);
    }

    protected void getFile(HttpServletResponse response, String fileId) {
        String filePath = null;
        if (fileId.endsWith(".ts")) {
            String[] tsAarray = fileId.split("_");
            String realFileId = tsAarray[0];
            //根据原文件的id查询出一个文件集合
            LambdaQueryWrapper<Video> queryWrapper = new LambdaQueryWrapper<>();
//            queryWrapper.eq(Video::getUserId,userId);
            queryWrapper.eq(Video::getId, realFileId);
            Video video = videoService.getOne(queryWrapper);
            if (video == null) {
                //分享的视频，ts路径记录的是原视频的id,这里通过id直接取出原视频
                LambdaQueryWrapper<Video> queryWrapper1 = new LambdaQueryWrapper<>();
                queryWrapper1.eq(Video::getId, fileId);
                List<Video> fileInfoList = videoService.list(queryWrapper1);

                video = fileInfoList.get(0);
                if (video == null) {
                    return;
                }

                //更具当前用户id和路径去查询当前用户是否有该文件，如果没有直接返回
//                fileInfoQuery = new FileInfoQuery();
//                fileInfoQuery.setFilePath(fileInfo.getFilePath());
//                fileInfoQuery.setUserId(userId);
                LambdaQueryWrapper<Video> queryWrapper2 = new LambdaQueryWrapper<>();
                queryWrapper2.eq(Video::getUrl, video.getUrl());
//                queryWrapper2.eq(Video::getUserId,userId);
                Integer count = videoService.count(queryWrapper2);
//                Integer count = fileInfoService.findCountByParam(fileInfoQuery);
                if (count == 0) {
                    return;
                }
            }
            String fileName = video.getUrl();
            fileName = StringTools.getFileNameNoSuffix(fileName) + "/" + fileId;
            filePath = appConfig.getProjectFolder() + Constants.FILE_FOLDER_FILE + fileName;
        } else {

            LambdaQueryWrapper<Video> queryWrapper = new LambdaQueryWrapper<>();
//            queryWrapper.eq(Video::getUserId,userId);
            queryWrapper.eq(Video::getId, fileId);
            List<Video> list = videoService.list(queryWrapper);
//            Video one = videoService.getOne(queryWrapper);
//            FileInfo fileInfo = fileInfoService.getFileInfoByFileIdAndUserId(fileId, userId);
            if (list.get(0) == null) {
                return;
            }
            filePath = appConfig.getProjectFolder() + Constants.FILE_FOLDER_FILE + list.get(0).getUrl();

        }
        File file = new File(filePath);
        if (!file.exists()) {
            return;
        }
        System.out.println(filePath);
        readFile(response, filePath);
    }


//    protected void download(HttpServletRequest request, HttpServletResponse response, String code) throws Exception {
//        DownloadFileDto downloadFileDto = redisComponent.getDownloadCode(code);
//        if (null == downloadFileDto) {
//            return;
//        }
//        String filePath = appConfig.getProjectFolder() + Constants.FILE_FOLDER_FILE + downloadFileDto.getFilePath();
//        String fileName = downloadFileDto.getFileName();
//        response.setContentType("application/x-msdownload; charset=UTF-8");
//        if (request.getHeader("User-Agent").toLowerCase().indexOf("msie") > 0) {//IE浏览器
//            fileName = URLEncoder.encode(fileName, "UTF-8");
//        } else {
//            fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
//        }
//        response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");
//        readFile(response, filePath);
//    }
}
