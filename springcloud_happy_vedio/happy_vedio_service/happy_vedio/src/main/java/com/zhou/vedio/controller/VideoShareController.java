package com.zhou.vedio.controller;


import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.vedio.service.VideoShareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/share")
public class VideoShareController {

    @Autowired
    private VideoShareService videoShareService;

    /**
     * 分享视频
     * @param receiveId 接受人的id
     * @param videoId 视频id
     * @return
     */
    @RequestMapping("/shareVideo")
    public ResponseResult shareVideo(Long receiveId,Long videoId){
        videoShareService.shareVideo(receiveId,videoId);
        return ResponseResult.okResult("分享成功");
    }
}
