package com.zhou.vedio.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhou.model.vedio.pojos.VideoShare;
import org.springframework.stereotype.Service;

@Service
public interface VideoShareService extends IService<VideoShare> {
    void shareVideo(Long receiveId,Long videoId);
}
