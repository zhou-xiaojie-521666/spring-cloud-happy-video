package com.zhou.vedio.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhou.model.chat.pojos.Danmu;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DanmuService extends IService<Danmu> {
    List<Danmu> getDanumList(Long pid);
}


