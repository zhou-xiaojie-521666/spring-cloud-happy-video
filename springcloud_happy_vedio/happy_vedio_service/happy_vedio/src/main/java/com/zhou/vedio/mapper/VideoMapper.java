package com.zhou.vedio.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhou.model.vedio.dto.SearchArticleVo;
import com.zhou.model.vedio.pojos.Video;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface VideoMapper extends BaseMapper<Video> {

//    void updateVideoStatusWithOldStatus(@Param("fileId") Long fileId, @Param("userId") int userId, @Param("video") Video video,
//                                       @Param("oldStatus") Integer oldStatus);


    public List<SearchArticleVo> loadArticleList();
}
