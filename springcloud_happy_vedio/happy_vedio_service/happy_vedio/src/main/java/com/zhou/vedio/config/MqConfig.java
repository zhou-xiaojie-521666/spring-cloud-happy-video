package com.zhou.vedio.config;

import com.zhou.model.MqContants;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class MqConfig {

    @Bean
    public TopicExchange topicExchange(){
        return new TopicExchange(MqContants.VIDEO_EXCHANGE,true,false);
    }

    @Bean
    public Queue insertQueue(){
        return new Queue(MqContants.VIDEO_INSERT_QUEUE,true);
    }

    @Bean
    public Queue updateQueue(){
        return new Queue(MqContants.VIDEO_UPDATE_QUEUE,true);
    }

    @Bean
    public Queue deleteQueue(){
        return new Queue(MqContants.VIDEO_DELETE_QUEUE,true);
    }

    @Bean
    public Binding insertQueueBinding(){
        return BindingBuilder.bind(insertQueue()).to(topicExchange()).with(MqContants.VIDEO_INSERT_KEY);
    }

    @Bean
    public Binding deleteQueueBinding(){
        return BindingBuilder.bind(deleteQueue()).to(topicExchange()).with(MqContants.VIDEO_DELETE_KEY);
    }

    @Bean
    public Binding updateQueueBinding(){
        return BindingBuilder.bind(updateQueue()).to(topicExchange()).with(MqContants.VIDEO_UPDATE_KEY);
    }
}
