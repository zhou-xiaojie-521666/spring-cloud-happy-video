package com.zhou.vedio.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhou.model.vedio.dto.UploadResultDto;
import com.zhou.model.vedio.pojos.Video;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public interface VideoService extends IService<Video> {

    UploadResultDto uploadMedia(Integer userId, Long fileId, MultipartFile file,String videoTitle,String videoDesc,List<String> typeCheck, Integer allowPost, Integer allowLook, String postTime, String detailPostTime) throws Exception;

    List<Video> getAllVideo();

    List<Video> getUserIdListVideo(List<Long> userIds);

    List<Video> getVideoMyPost(Integer id);


    UploadResultDto uploadFile(int intUserId, Long fileId, MultipartFile file,
                               String fileName,
                               String fileMd5,
                               Integer chunkIndex,
                               Integer chunks,
                               String videoTitle,
                               String videoDesc,
                               List<String> typeCheck,
                               Integer allowPost,
                               Integer allowLook,
                               String postTime,
                               String DetailPostTime);

    void transferFile(Long fileId, int intUserId);

    Video getVideoInfoById(Long id);

    Video clientVideoById(Long id);


    void deleteVideo(Long videoId);

    void hideVideo(Long videoId);

    void showVideo(Long videoId);
}
