package com.zhou.vedio.utils;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.*;
import com.zhou.vedio.config.OSSConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class OSSUtil {

    private final OSSConfig ossConfig;

    public String uploadVideo(MultipartFile uploadFile) throws Exception {
        if (!ossConfig.isEnable()) {
            return "";
        }
        // OSS客户端初始化
        OSS ossClient = new OSSClientBuilder().build(ossConfig.getEndpoint(), ossConfig.getAccesskeyid(), ossConfig.getAccesskeysecret());
        // 生成基于时间戳的文件名
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String date = sf.format(new Date());
        String random = UUID.randomUUID().toString().substring(0, 8);
        String fileName = date + random;

        // 确定分片大小
        final long partSize = 5 * 1024 * 1024L; // 设置分片大小为5MB
        // 初始化分片上传请求
        InitiateMultipartUploadRequest initiateRequest = new InitiateMultipartUploadRequest(ossConfig.getBucketname(), ossConfig.getKey() + date + "/" + fileName);
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType(uploadFile.getContentType());
        initiateRequest.setObjectMetadata(metadata);
        InitiateMultipartUploadResult initiateResult = ossClient.initiateMultipartUpload(initiateRequest);
        String uploadId = initiateResult.getUploadId();

        // 准备分片上传
        List<PartETag> partETags = new ArrayList<PartETag>();

        // 计算文件有多少个分片
        final long contentLength = uploadFile.getSize();
        int partCount = (int) (contentLength / partSize);
        if (contentLength % partSize != 0) {
            partCount++;
        }

         // 循环创建上传每一个分片
        for (int i = 0; i < partCount; i++) {
            long startPos = i * partSize;
            long curPartSize = (i + 1 == partCount) ? (contentLength - startPos) : partSize;

            // 创建上传分片请求
            UploadPartRequest uploadPartRequest = new UploadPartRequest();
            uploadPartRequest.setBucketName(ossConfig.getBucketname());
            uploadPartRequest.setKey(ossConfig.getKey() + date + "/" + fileName);
            uploadPartRequest.setUploadId(uploadId);
            uploadPartRequest.setInputStream(new ByteArrayInputStream(uploadFile.getBytes()));
            uploadPartRequest.setPartSize(curPartSize);
            uploadPartRequest.setPartNumber(i + 1);

            // 上传分片并收集返回的分片编号和ETag
            UploadPartResult uploadPartResult = ossClient.uploadPart(uploadPartRequest);
            partETags.add(uploadPartResult.getPartETag());
        }

        // 完成分片上传
        CompleteMultipartUploadRequest completeMultipartUploadRequest = new CompleteMultipartUploadRequest(ossConfig.getBucketname(), ossConfig.getKey() + date + "/" + fileName, uploadId, partETags);
        ossClient.completeMultipartUpload(completeMultipartUploadRequest);

        // 关闭OSS客户端
        ossClient.shutdown();

        return String.format("https://%s.%s/%s/%s", ossConfig.getBucketname(), ossConfig.getHost(), ossConfig.getKey() + date, fileName);
    }


    public String picOSS(MultipartFile uploadFile) throws Exception {
        if (!ossConfig.isEnable()) {
            return "";
        }
        // 创建OSSClient实例
        OSSClient ossClient = new OSSClient(ossConfig.getEndpoint(), ossConfig.getAccesskeyid(), ossConfig.getAccesskeysecret());
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        // 上传
        long time = new Date().getTime();
        String date = sf.format(time);
        // 上传的文件名，这里使用时间戳作为文件名的一部分，保证唯一性
        // 注意，确保随机生成的数字或时间戳符合你的要求
        String timestamp = String.valueOf(time);
        String fileExtension = getcontentType(uploadFile.getOriginalFilename().substring(uploadFile.getOriginalFilename().lastIndexOf(".")));
        String fileName = timestamp + fileExtension;

        //设置请求头
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentType(fileExtension);
        // 上传开始
        ossClient.putObject(ossConfig.getBucketname(), ossConfig.getKey() + date + "/" + fileName, new ByteArrayInputStream(uploadFile.getBytes()), objectMetadata);
        // 关闭client
        ossClient.shutdown();
        return String.format("https://%s.%s/%s/%s", ossConfig.getBucketname(), ossConfig.getHost(), ossConfig.getKey() + date, fileName);
    }


    public void deletePicOSS(String picUrl) throws Exception {
        if (!ossConfig.isEnable()) {
            return;
        }

//        // 提取图片的 key（文件路径）
//        String key = getImageKeyFromUrl(picUrl);

        // 创建 OSSClient 实例
        OSSClient ossClient = new OSSClient(ossConfig.getEndpoint(), ossConfig.getAccesskeyid(), ossConfig.getAccesskeysecret());

        // 删除图片
        ossClient.deleteObject(ossConfig.getBucketname(), picUrl);
        System.out.println("删除了图片"+picUrl);
        // 关闭 client
        ossClient.shutdown();
    }



    //根据文件的类型 设置请求头
    public static String getcontentType(String FilenameExtension) {
        if (FilenameExtension.equalsIgnoreCase("video/mp4")) {
            return "video/mp4";
        }
        if (FilenameExtension.equalsIgnoreCase(".bmp")) {
            return "image/bmp";
        }
        if (FilenameExtension.equalsIgnoreCase(".gif")) {
            return "image/gif";
        }
        if (FilenameExtension.equalsIgnoreCase(".jpeg") ||
                FilenameExtension.equalsIgnoreCase(".jpg") ||
                FilenameExtension.equalsIgnoreCase(".png")) {
            return "image/jpg";
        }
        if (FilenameExtension.equalsIgnoreCase(".html")) {
            return "text/html";
        }
        if (FilenameExtension.equalsIgnoreCase(".txt")) {
            return "text/plain";
        }
        if (FilenameExtension.equalsIgnoreCase(".vsd")) {
            return "application/vnd.visio";
        }
        if (FilenameExtension.equalsIgnoreCase(".pptx") ||
                FilenameExtension.equalsIgnoreCase(".ppt")) {
            return "application/vnd.ms-powerpoint";
        }
        if (FilenameExtension.equalsIgnoreCase(".docx") ||
                FilenameExtension.equalsIgnoreCase(".doc")) {
            return "application/msword";
        }
        if (FilenameExtension.equalsIgnoreCase(".xml")) {
            return "text/xml";
        }
        return "image/jpg";
    }
}
