package com.zhou.vedio.feign;

import com.zhou.model.chat.pojos.UserMessage;
import com.zhou.model.common.dtos.ResponseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("happy-chat")
public interface MessageClient {

    @RequestMapping("/api/chat/insertMessage")
    public ResponseResult insertMessage(UserMessage userMessage);
}
