package com.zhou.vedio.feign;


import com.zhou.model.user.pojos.UserInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("happy-user")
public interface UserClient {
    @RequestMapping("/api/user/getUserById/{id}")
    UserInfo getUserInfoById(@PathVariable Long id);
}
