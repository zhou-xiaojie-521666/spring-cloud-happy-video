package com.zhou.vedio.controller;

import com.zhou.common.exception.BusinessException;
import com.zhou.model.MqContants;
import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.model.vedio.dto.SearchArticleVo;
import com.zhou.model.vedio.dto.UploadResultDto;
import com.zhou.model.vedio.enums.UploadStatusEnums;
import com.zhou.model.vedio.pojos.Video;
import com.zhou.utils.AppJwtUtil;
import com.zhou.vedio.service.VideoService;
import io.jsonwebtoken.Claims;
import io.swagger.models.auth.In;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/vedio")
public class VideoController extends CommonFileController{

    @Autowired
    private VideoService videoService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @RequestMapping("/getVedio")
    public String getVedio(){
        return "hello";
    }

    /**
     * 将视频上传到云端
     * @param token
     * @param fileId
     * @param file
     * @param videoTitle
     * @param videoDesc
     * @param typeCheck
     * @param allowPost
     * @param allowLook
     * @param postTime
     * @param DetailPostTime
     * @return
     * @throws Exception
     */
    @RequestMapping("/uploadMedia")
    public ResponseResult uploadMedia(@RequestHeader("token") String token,
                                      Long fileId,
                                      MultipartFile file,
                                      String videoTitle,
                                      String videoDesc,
                                      @RequestParam("typeCheck") List<String> typeCheck,
                                      Integer allowPost,
                                      Integer allowLook,
                                      String postTime,
                                      String DetailPostTime) throws Exception {
        Claims claimsBody = AppJwtUtil.getClaimsBody(token);
        //获取用户信息
        Object userId = claimsBody.get("id");
        if (userId==null){
            throw new BusinessException("用户身份过期");
        }else {
            int intUserId;
            try {
                intUserId = Integer.parseInt(userId.toString());
                UploadResultDto uploadResultDto = videoService.uploadMedia(intUserId, fileId, file, videoTitle, videoDesc, typeCheck, allowPost, allowLook, postTime, DetailPostTime);
                return ResponseResult.okResult(uploadResultDto);
            } catch (NumberFormatException e) {
                throw new BusinessException("用户ID格式不正确");
            }
        }
    }


    /**
     * 将视频上传到本地
     * @param token
     * @param fileId
     * @param file
     * @param fileName
     * @param fileMd5
     * @param chunkIndex
     * @param chunks
     * @param videoTitle
     * @param videoDesc
     * @param typeCheck
     * @param allowPost
     * @param allowLook
     * @param postTime
     * @param DetailPostTime
     * @return
     */
    @RequestMapping("/uploadMediaToLocal")
    public ResponseResult uploadMediaToLocal(@RequestHeader("token") String token,
                                             Long fileId,
                                             MultipartFile file,
                                             String fileName,
                                             String fileMd5,
                                             Integer chunkIndex,
                                             Integer chunks,
                                             String videoTitle,
                                             String videoDesc,
                                             @RequestParam("typeCheck") List<String> typeCheck,
                                             Integer allowPost,
                                             Integer allowLook,
                                             String postTime,
                                             String DetailPostTime
    ){
        Claims claimsBody = AppJwtUtil.getClaimsBody(token);
        //获取用户信息
        Object userId = claimsBody.get("id");
        if (userId==null){
            throw new BusinessException("用户身份过期");
        }else {
            int intUserId;
            try {
                intUserId = Integer.parseInt(userId.toString());
                UploadResultDto resultDto = videoService.uploadFile(intUserId, fileId, file, fileName, fileMd5,
                        chunkIndex, chunks,videoTitle, videoDesc, typeCheck, allowPost, allowLook, postTime, DetailPostTime);
                if (resultDto.getStatus() == UploadStatusEnums.UPLOAD_FINISH.getCode()||
                        resultDto.getStatus() == UploadStatusEnums.UPLOAD_SECONDS.getCode()){
                    rabbitTemplate.convertAndSend(MqContants.VIDEO_EXCHANGE,MqContants.VIDEO_INSERT_KEY,resultDto.getSearchArticleVo());
                }
                return ResponseResult.okResult(resultDto);
            } catch (NumberFormatException e) {
                throw new BusinessException("用户ID格式不正确");
            }
        }

    }


    @RequestMapping("/getAllVideo")
    public ResponseResult getAllVideo(){
        return ResponseResult.okResult(videoService.getAllVideo());
    }

    @RequestMapping("/getUserIdListVideo")
    /**
     * 获取传过来的用户id发的视频
     * @param userIds
     * @return
     */
    public ResponseResult getUserIdListVideo(@RequestParam("userIds") List<Long> userIds){
        return ResponseResult.okResult(videoService.getUserIdListVideo(userIds));
    }


    @RequestMapping("/getVideoMyPost/{id}")
    public ResponseResult getVideoMyPost(@PathVariable Integer id){
        List<Video> videoMyPost = videoService.getVideoMyPost(id);
        return ResponseResult.okResult(videoMyPost);
    }


    /**
     * 获取图片
     * @param response
     * @param imageFolder
     * @param imageName
     */
    @RequestMapping("/getImage/{imageFolder}/{imageName}")
    public void getImage(HttpServletResponse response, @PathVariable("imageFolder") String imageFolder, @PathVariable("imageName") String imageName) {
        super.getImage(response, imageFolder, imageName);
    }

    /**
     * 获取视频
     * @param response
     * @param fileId
     */
    @RequestMapping("/ts/getVideoInfo/{fileId}")
    public void getVideoInfo(HttpServletResponse response, @PathVariable("fileId") String fileId) {
        super.getFile(response, fileId);
    }


    @RequestMapping("/getVideoInfoById/{id}")
    public ResponseResult getVideoInfoById(@PathVariable Long id){
        Video video = videoService.getVideoInfoById(id);
        return ResponseResult.okResult(video);
    }


    @RequestMapping("/clientVideoById/{id}")
    public Video clientVideoById(@PathVariable Long id){
        Video video = videoService.clientVideoById(id);
        return video;
    }

    @RequestMapping("/deleteVideo")
    public ResponseResult deleteVideo(Long videoId){
        videoService.deleteVideo(videoId);
        return ResponseResult.okResult("删除成功");
    }

    @RequestMapping("/hideVideo")
    public ResponseResult hideVideo(Long videoId){
        videoService.hideVideo(videoId);
        return ResponseResult.okResult("隐藏成功");
    }

    @RequestMapping("/showVideo")
    public ResponseResult showVideo(Long videoId){
        videoService.showVideo(videoId);
        return ResponseResult.okResult("展出成功");
    }


}
