package com.zhou.vedio.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhou.model.vedio.pojos.VideoShare;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface VideoShareMapper extends BaseMapper<VideoShare> {
}
