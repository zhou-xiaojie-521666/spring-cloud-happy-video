package com.zhou.vedio.config;

import com.zhou.model.MqContants;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MqMessageConfig {
    @Bean
    public TopicExchange MessageTopicExchange(){
        return new TopicExchange(MqContants.MESSAGE_EXCHANGE,true,false);
    }

    @Bean
    public Queue MessageInsertQueue(){
        return new Queue(MqContants.MESSAGE_INSERT_QUEUE,true);
    }


    @Bean
    public Queue MessageDeleteQueue(){
        return new Queue(MqContants.MESSAGE_DELETE_QUEUE,true);
    }

    @Bean
    public Binding MessageInsertQueueBinding(){
        return BindingBuilder.bind(MessageInsertQueue()).to(MessageTopicExchange()).with(MqContants.MESSAGE_INSERT_KEY);
    }

    @Bean
    public Binding MessageDeleteQueueBinding(){
        return BindingBuilder.bind(MessageDeleteQueue()).to(MessageTopicExchange()).with(MqContants.MESSAGE_DELETE_KEY);
    }
}
