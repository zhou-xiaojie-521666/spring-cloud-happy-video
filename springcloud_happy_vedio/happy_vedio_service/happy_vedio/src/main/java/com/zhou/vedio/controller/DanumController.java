package com.zhou.vedio.controller;

import com.zhou.model.chat.pojos.Danmu;
import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.vedio.service.DanmuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/danmu")
public class DanumController {

    @Autowired
    private DanmuService danmuService;

    @RequestMapping("/getDanumList/{pid}")
    public ResponseResult getDanumList(@PathVariable Long pid){
        List<Danmu> danumList = danmuService.getDanumList(pid);
        return ResponseResult.okResult(danumList);
    }
}
