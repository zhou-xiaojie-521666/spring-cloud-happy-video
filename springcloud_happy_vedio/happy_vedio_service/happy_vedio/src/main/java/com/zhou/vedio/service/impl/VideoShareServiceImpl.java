package com.zhou.vedio.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhou.common.exception.BusinessException;
import com.zhou.model.MqContants;
import com.zhou.model.chat.enums.MessageTypeEnums;
import com.zhou.model.chat.pojos.UserMessage;
import com.zhou.model.user.pojos.UserInfo;
import com.zhou.model.vedio.pojos.Video;
import com.zhou.model.vedio.pojos.VideoShare;
import com.zhou.utils.thread.AppThreadLocalUtil;
import com.zhou.vedio.feign.MessageClient;
import com.zhou.vedio.mapper.VideoShareMapper;
import com.zhou.vedio.service.VideoService;
import com.zhou.vedio.service.VideoShareService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
@Transactional
public class VideoShareServiceImpl extends ServiceImpl<VideoShareMapper, VideoShare> implements VideoShareService {

    @Autowired
    private MessageClient messageClient;

    @Autowired
    private VideoService videoService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Transactional
    @Override
    public void shareVideo(Long receiveId, Long videoId) {

        //获取线程中的用户信息
        UserInfo user = AppThreadLocalUtil.getUser();
        if (user==null){
            throw new BusinessException("未登录");
        }
        LambdaQueryWrapper<Video> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(Video::getPid,videoId);
        Video video = videoService.getOne(queryWrapper);
        //将分享加入数据库
        VideoShare videoShare=new VideoShare();
        videoShare.setVideoId(videoId);
        videoShare.setReceiveId(receiveId);
        videoShare.setSendId(user.getId());
        videoShare.setVid(video.getId());
        videoShare.setPid(video.getPid());
        videoShare.setGmtCreated(new Date());
        videoShare.setGmtUpdated(new Date());

        save(videoShare);
        //给接收人发送一条系统通知
        UserMessage userMessage=new UserMessage();
        userMessage.setFromId(user.getId());
        userMessage.setToId(receiveId);
        userMessage.setMessage("有人向你转发了一条有趣的视频！");
        userMessage.setImage(video.getCover());
        userMessage.setType(MessageTypeEnums.INFORM.getType());
        userMessage.setCreateTime(new Date());
//        messageClient.insertMessage(userMessage);
        rabbitTemplate.convertAndSend(MqContants.MESSAGE_EXCHANGE,MqContants.MESSAGE_INSERT_KEY,userMessage);
        //给接收人的私信发送一条消息
        UserMessage userMessageTwo=new UserMessage();
        userMessageTwo.setFromId(user.getId());
        userMessageTwo.setToId(receiveId);
        userMessageTwo.setImage(video.getCover());
        userMessageTwo.setType(MessageTypeEnums.SHARE.getType());
        userMessageTwo.setCreateTime(new Date());
//        messageClient.insertMessage(userMessageTwo);
        rabbitTemplate.convertAndSend(MqContants.MESSAGE_EXCHANGE,MqContants.MESSAGE_INSERT_KEY,userMessageTwo);
    }
}
