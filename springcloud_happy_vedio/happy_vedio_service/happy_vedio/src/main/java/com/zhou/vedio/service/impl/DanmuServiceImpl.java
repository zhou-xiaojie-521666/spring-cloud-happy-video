package com.zhou.vedio.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhou.common.exception.BusinessException;
import com.zhou.model.chat.pojos.Danmu;
import com.zhou.vedio.mapper.DanmuMapper;
import com.zhou.vedio.service.DanmuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DanmuServiceImpl extends ServiceImpl<DanmuMapper, Danmu> implements DanmuService {

    @Autowired
    private DanmuMapper danmuMapper;

    @Override
    public List<Danmu> getDanumList(Long pid) {
        if (pid==null){
            throw new BusinessException("视频不存在");
        }
        LambdaQueryWrapper<Danmu> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(Danmu::getVid, pid);
        return list(queryWrapper);
    }
}
