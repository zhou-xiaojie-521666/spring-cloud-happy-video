package com.zhou.vedio.feign;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("happy-comment")
public interface CommentClient {

    @RequestMapping("/api/comment/getVideoCommentCount/{id}")
    public Long getVideoCommentCount(@PathVariable Long id);
}
