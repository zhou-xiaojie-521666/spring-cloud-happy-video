package com.zhou.vedio.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhou.model.chat.pojos.Danmu;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DanmuMapper extends BaseMapper<Danmu> {
}
