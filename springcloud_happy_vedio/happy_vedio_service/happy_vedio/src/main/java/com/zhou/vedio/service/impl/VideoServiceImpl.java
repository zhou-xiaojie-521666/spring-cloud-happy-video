package com.zhou.vedio.service.impl;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.zhou.apis.schedule.ISearchFeignClient;
import com.zhou.common.exception.BusinessException;
import com.zhou.model.Constants;
import com.zhou.model.MqContants;
import com.zhou.model.user.pojos.UserInfo;
import com.zhou.model.vedio.dto.SearchArticleVo;
import com.zhou.model.vedio.dto.UploadResultDto;
import com.zhou.model.vedio.enums.AuditQueueStatusEnum;
import com.zhou.model.vedio.enums.FileStatusEnums;
import com.zhou.model.vedio.enums.UploadStatusEnums;
import com.zhou.model.vedio.pojos.Video;
import com.zhou.utils.StringTools;
import com.zhou.utils.thread.AppThreadLocalUtil;
import com.zhou.vedio.config.AppConfig;
import com.zhou.vedio.feign.CommentClient;
import com.zhou.vedio.feign.UserClient;
import com.zhou.vedio.mapper.VideoMapper;
import com.zhou.vedio.service.VideoService;
import com.zhou.vedio.utils.*;
import io.swagger.models.auth.In;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class VideoServiceImpl extends ServiceImpl<VideoMapper, Video> implements VideoService {

    private static final Logger logger = LoggerFactory.getLogger(VideoServiceImpl.class);

    @Resource
    @Lazy
    private VideoService videoService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private OSSUtil ossUtil;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private AppConfig appConfig;

    @Autowired
    private UserClient userClient;

    @Autowired
    private CommentClient commentClient;

    @Autowired
    private VideoMapper videoMapper;

//    @Autowired
//    private ISearchFeignClient searchFeignClient;


    @Transactional
    @Override
    public UploadResultDto uploadMedia(Integer userId, Long fileId, MultipartFile file,String videoTitle,String videoDesc,List<String> typeCheck, Integer allowPost, Integer allowLook, String postTime, String detailPostTime) throws Exception {

        String vedioUrl = ossUtil.uploadVideo(file);
        if (vedioUrl==null){
            throw new BusinessException("视频上传错误，请联系管理员");
        }
        UploadResultDto uploadResultDto=new UploadResultDto();
        Video video=new Video();
        video.setId(fileId);
        video.setFileSize(file.getSize());
        video.setTitle(videoTitle);
        video.setDescription(videoDesc);
        video.setUrl(vedioUrl);
        video.setUserId(userId);

        video.setOpen(allowLook);
        video.setShareCount(0L);
        video.setStartCount(0L);
        video.setHistoryCount(0L);
        video.setFavoritesCount(0L);

        //逻辑删除
        video.setIsDeleted(0);
        video.setGmtCreated(new Date());
        video.setGmtUpdated(new Date());
        //TODO 视频的分类
        String tagJson = JSON.toJSONString(typeCheck);
        video.setTagList(tagJson);
        //立刻发布
        if (postTime=="0"){
            video.setPostTime(postTime);
            video.setDetailPostTime(new Date().toString());
            video.setAuditStatus(0);
        }
        //加入计时器进行延迟发布
        if (postTime=="1"){
            video.setPostTime(postTime);
            video.setDetailPostTime(detailPostTime);
            //TODO 加入数据库进行延迟审核队列
            video.setAuditStatus(0);
            video.setAuditQueueStatus(AuditQueueStatusEnum.AUDITING.getStatus());
        }
        boolean save = save(video);

        if (!save){
            throw new BusinessException("视频上传失败，原视频可以已经存在了");
        }

        uploadResultDto.setFileId(fileId);
        uploadResultDto.setUrl(vedioUrl);

        //TODO 需要更改
        uploadResultDto.setStatus(AuditQueueStatusEnum.ABLE.getStatus());
        return uploadResultDto;

    }

    @Override
    public List<Video> getAllVideo() {
        LambdaQueryWrapper<Video> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.isNotNull(Video::getFileName);
        queryWrapper.eq(Video::getIsDeleted,0);
        List<Video> list = list(queryWrapper);
        list=list.stream().map((item)->{
            Integer userId = item.getUserId();
            UserInfo userInfoById = userClient.getUserInfoById(Long.valueOf(userId));
            item.setUserCover(userInfoById.getAvatar());
            item.setNickName(userInfoById.getNickName());
            Long videoCommentCount = commentClient.getVideoCommentCount(item.getPid());
            item.setCommentCount(videoCommentCount);
            return item;
        }).collect(Collectors.toList());

        HashMap<Long, Map<String, Long>> map = new HashMap<>();
        for (Video video : list) {
            // 创建一个存储点赞数、分享数、浏览次数、收藏次数的 Map
            Map<String, Long> countsMap = new HashMap<>();
            // 将点赞数、分享数、浏览次数、收藏次数放入 countsMap
            countsMap.put("startCount", video.getStartCount());
            countsMap.put("shareCount", video.getShareCount());
            countsMap.put("historyCount", video.getHistoryCount());
            countsMap.put("favoritesCount", video.getFavoritesCount());
            map.put(video.getId(), countsMap);
        }
        Collections.shuffle(list);
        redisUtil.set("VideoBehavior",map,3600*24);

        redisUtil.set("allVideo",list,3600*24);
        return list;
    }

    @Override
    public List<Video> getUserIdListVideo(List<Long> userIds) {
        List<Video> finalList = new ArrayList<>();
        userIds.forEach(userId->{
            LambdaQueryWrapper<Video> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.isNotNull(Video::getFileName);
            queryWrapper.eq(Video::getUserId, userId);
            // 从数据库中查询当前用户ID的视频列表
            List<Video> list = list(queryWrapper);
            List<Video> updatedList = list.stream().map(item -> { // 遍历查询到的视频列表
                Integer id = item.getUserId();
                UserInfo userInfoById = userClient.getUserInfoById(Long.valueOf(userId));
                item.setUserCover(userInfoById.getAvatar()); // 设置视频封面
                item.setNickName(userInfoById.getNickName());
                Long videoCommentCount = commentClient.getVideoCommentCount(item.getId());
                item.setCommentCount(videoCommentCount); // 更新评论计数
                return item; // 返回更新后的视频信息
            }).collect(Collectors.toList());
            finalList.addAll(updatedList);
        });

        return finalList;
    }

    @Override
    public List<Video> getVideoMyPost(Integer id) {
        LambdaQueryWrapper<Video> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(Video::getUserId,id);
        queryWrapper.isNotNull(Video::getFileName);
        return list(queryWrapper);
    }



    @Transactional(rollbackFor = Exception.class)
    @Override
    public UploadResultDto uploadFile(int intUserId, Long fileId,
                                      MultipartFile file,
                                      String fileName,
                                      String fileMd5,
                                      Integer chunkIndex,
                                      Integer chunks,
                                      String videoTitle,
                                      String videoDesc,
                                      List<String> typeCheck,
                                      Integer allowPost,
                                      Integer allowLook,
                                      String postTime,
                                      String detailPostTime) {
        File tempFileFolder = null;
        Boolean uploadSuccess = true;
        SearchArticleVo searchArticleVo=new SearchArticleVo();

        try {
            UploadResultDto resultDto = new UploadResultDto();

            resultDto.setFileId(fileId);
            Date curDate = new Date();

            if (chunkIndex == 0) {
                //秒传
                LambdaQueryWrapper<Video> queryWrapper=new LambdaQueryWrapper<>();
                queryWrapper.eq(Video::getFileMd5,fileMd5);
                List<Video> dbFileList = list(queryWrapper);
                //秒传
                if (!dbFileList.isEmpty()) {
                    Video dbFile = dbFileList.get(0);
                    dbFile.setPid(null);
                    dbFile.setUserId(intUserId);
                    dbFile.setFileSize(file.getSize());
                    dbFile.setTitle(videoTitle);
                    dbFile.setDescription(videoDesc);
                    dbFile.setGmtCreated(curDate);
                    dbFile.setGmtUpdated(curDate);
                    dbFile.setOpen(allowLook);
                    dbFile.setShareCount(0L);
                    dbFile.setStartCount(0L);
                    dbFile.setHistoryCount(0L);
                    dbFile.setFavoritesCount(0L);
                    //TODO 视频的分类
                    String tagJson = JSON.toJSONString(typeCheck);
                    System.out.println(tagJson);
                    dbFile.setTagList(tagJson);
                    //立刻发布
                    if (postTime=="0"){
                        dbFile.setPostTime(postTime);
                        dbFile.setDetailPostTime(new Date().toString());
                        dbFile.setAuditStatus(0);
                    }
                    //加入计时器进行延迟发布
                    if (postTime=="1"){
                        dbFile.setPostTime(postTime);
                        dbFile.setDetailPostTime(detailPostTime);
                        //TODO 加入数据库进行延迟审核队列
                        dbFile.setAuditStatus(0);
                        dbFile.setAuditQueueStatus(AuditQueueStatusEnum.AUDITING.getStatus());
                    }
                    dbFile.setFileMd5(fileMd5);

                    save(dbFile);
                    //TODO
                    searchArticleVo.setPid(dbFile.getPid());
                    searchArticleVo.setId(dbFile.getId());
                    searchArticleVo.setTitle(dbFile.getTitle());
                    searchArticleVo.setGmtCreated(dbFile.getGmtCreated());
                    searchArticleVo.setUserId((long) intUserId);
                    searchArticleVo.setDescription(dbFile.getDescription());
                    searchArticleVo.setStartCount(0L);
                    searchArticleVo.setShareCount(0L);
                    searchArticleVo.setFavoritesCount(0L);
                    searchArticleVo.setIsDeleted(0);
                    resultDto.setSearchArticleVo(searchArticleVo);
                    resultDto.setStatus(UploadStatusEnums.UPLOAD_SECONDS.getCode());
                    return resultDto;
                }
            }
            //暂存在临时目录
            String tempFolderName = appConfig.getProjectFolder() + Constants.FILE_FOLDER_TEMP;

            Long currentUserFolderName = intUserId + fileId;
            //创建临时目录
            tempFileFolder = new File(tempFolderName + currentUserFolderName);
            if (!tempFileFolder.exists()) {
                tempFileFolder.mkdirs();
            }

            File newFile = new File(tempFileFolder.getPath() + "/" + chunkIndex);
            file.transferTo(newFile);

            //不是最后一个分片，直接返回
            if (chunkIndex < chunks - 1) {
                resultDto.setStatus(UploadStatusEnums.UPLOADING.getCode());
                return resultDto;
            }
            //最后一个分片上传完成，记录数据库，异步合并分片
            String month = DateUtil.format(curDate, "yyyyMM");
            String fileSuffix = StringTools.getFileSuffix(fileName);
            //真实文件名
            String realFileName = currentUserFolderName + fileSuffix;
            Video videoInfo = new Video();

            videoInfo.setId(fileId);
            videoInfo.setUserId(intUserId);
            videoInfo.setFileMd5(fileMd5);
            videoInfo.setUrl(month + "/" + realFileName);
            videoInfo.setGmtCreated(curDate);
            videoInfo.setGmtUpdated(curDate);
            videoInfo.setOpen(allowLook);
            videoInfo.setFileSize(file.getSize());
            videoInfo.setTitle(videoTitle);
            videoInfo.setDescription(videoDesc);
            videoInfo.setShareCount(0L);
            videoInfo.setStartCount(0L);
            videoInfo.setHistoryCount(0L);
            videoInfo.setFavoritesCount(0L);
            videoInfo.setFileName(fileName);
            //TODO 视频的分类
            String tagJson = JSON.toJSONString(typeCheck);
            System.out.println(tagJson);
            videoInfo.setTagList(tagJson);
            //立刻发布
            if (postTime=="0"){
                videoInfo.setPostTime(postTime);
                videoInfo.setDetailPostTime(new Date().toString());
                videoInfo.setAuditStatus(0);
            }
            //加入计时器进行延迟发布
            if (postTime=="1"){
                videoInfo.setPostTime(postTime);
                videoInfo.setDetailPostTime(detailPostTime);
                //TODO 加入数据库进行延迟审核队列
                videoInfo.setAuditStatus(0);
                videoInfo.setAuditQueueStatus(AuditQueueStatusEnum.AUDITING.getStatus());
            }

            save(videoInfo);
            searchArticleVo.setPid(videoInfo.getPid());
            searchArticleVo.setId(videoInfo.getId());
            searchArticleVo.setTitle(videoInfo.getTitle());
            searchArticleVo.setGmtCreated(videoInfo.getGmtCreated());
            searchArticleVo.setUserId((long) intUserId);
            searchArticleVo.setDescription(videoInfo.getDescription());
            searchArticleVo.setStartCount(0L);
            searchArticleVo.setShareCount(0L);
            searchArticleVo.setFavoritesCount(0L);
            searchArticleVo.setIsDeleted(0);
            resultDto.setSearchArticleVo(searchArticleVo);
            resultDto.setStatus(UploadStatusEnums.UPLOAD_FINISH.getCode());
            //事务提交后调用异步方法
            TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronization() {
                @Override
                public void afterCommit() {
                    videoService.transferFile(fileId, intUserId);
                }
            });
            return resultDto;
        } catch (BusinessException e) {
            uploadSuccess = false;
            logger.error("文件上传失败", e);
            throw e;
        } catch (Exception e) {
            uploadSuccess = false;
            logger.error("文件上传失败", e);
            throw new BusinessException("文件上传失败");
        } finally {
            //如果上传失败，清除临时目录
            if (tempFileFolder != null && !uploadSuccess) {
                try {
                    FileUtils.deleteDirectory(tempFileFolder);
                } catch (IOException e) {
                    logger.error("删除临时目录失败");
                }
            }
        }
    }



    @Transactional
    @Async
    public void transferFile(Long fileId, int userId) {
        Boolean transferSuccess = true;
        String targetFilePath = null;
        String cover = null;
        LambdaQueryWrapper<Video> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(Video::getId,fileId);
        queryWrapper.eq(Video::getUserId,userId);
        Video fileInfo = getOne(queryWrapper);

        try {
            if (fileInfo == null ) {
                return;
            }
            //临时目录
            String tempFolderName = appConfig.getProjectFolder() + Constants.FILE_FOLDER_TEMP;

            Long  currentUserFolderName = userId + fileId;
            File fileFolder = new File(tempFolderName + currentUserFolderName);
            if (!fileFolder.exists()) {
                fileFolder.mkdirs();
            }
            //文件后缀
            String fileSuffix = StringTools.getFileSuffix(fileInfo.getFileName());
            String month = DateUtil.format(fileInfo.getGmtCreated(), "yyyyMM");
            //目标目录
            String targetFolderName = appConfig.getProjectFolder() + Constants.FILE_FOLDER_FILE;
            File targetFolder = new File(targetFolderName + "/" + month);
            if (!targetFolder.exists()) {
                targetFolder.mkdirs();
            }
            //真实文件名
            String realFileName = currentUserFolderName + fileSuffix;
            //真实文件路径
            targetFilePath = targetFolder.getPath() + "/" + realFileName;
            //合并文件
            union(fileFolder.getPath(), targetFilePath, fileInfo.getFileName(), true);

            cutFile4Video(fileId, targetFilePath);
            //视频生成缩略图
            cover = month + "/" + currentUserFolderName + Constants.IMAGE_PNG_SUFFIX;
            String coverPath = targetFolderName + "/" + cover;
            ScaleFilter.createCover4Video(new File(targetFilePath), Constants.LENGTH_150, new File(coverPath));

        } catch (Exception e) {
            logger.error("文件转码失败，文件Id:{},userId:{}", fileId, userId, e);
            transferSuccess = false;
        } finally {

            Video updateInfo = new Video();
//            updateInfo.setFileSize(new File(targetFilePath).length());
            updateInfo.setCover(cover);
            updateInfo.setStatus(transferSuccess ? FileStatusEnums.USING.getStatus() : FileStatusEnums.TRANSFER_FAIL.getStatus());
            LambdaUpdateWrapper<Video> updateWrapper = new LambdaUpdateWrapper<>();
            updateWrapper.eq(Video::getId, fileId);
            updateWrapper.set(Video::getStatus, 2);
            update(updateInfo, updateWrapper);
        }
    }

    @Override
    public Video getVideoInfoById(Long id) {
        LambdaQueryWrapper<Video> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(Video::getPid,id);
        Video one = getOne(queryWrapper);
        Integer userId = one.getUserId();
        UserInfo userInfoById = userClient.getUserInfoById(Long.valueOf(userId));
        one.setUserCover(userInfoById.getAvatar());
        one.setNickName(userInfoById.getNickName());
        Long videoCommentCount = commentClient.getVideoCommentCount(one.getId());
        one.setCommentCount(videoCommentCount);
        return one;
    }

    @Override
    public Video clientVideoById(Long id) {
        LambdaQueryWrapper<Video> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(Video::getPid,id);
        Video one = getOne(queryWrapper);
        return one;
    }

    @Override
    public void deleteVideo(Long videoId) {
        UserInfo user = AppThreadLocalUtil.getUser();
        LambdaQueryWrapper<Video> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(Video::getPid,videoId);
        Video one = getOne(queryWrapper);
        if (one.getUserId().longValue() != user.getId()){
            throw new BusinessException("权限不足");
        }
        boolean remove = remove(queryWrapper);
        if (remove){
            //TODO 查找文件中删除视频
            // 获取文件存储的目标路径
            String fileSuffix = StringTools.getFileSuffix(one.getFileName());
            String month = DateUtil.format(one.getGmtCreated(), "yyyyMM");
            String targetFolderName = appConfig.getProjectFolder() + Constants.FILE_FOLDER_FILE;
            String targetFilePath = targetFolderName + "/" + month + "/" + (user.getId() + one.getId() + fileSuffix);

            // 尝试删除文件系统中的文件
//            try {
//                File fileToDelete = new File(targetFilePath);
//                if (fileToDelete.exists()) {
//                    if (fileToDelete.delete()) {
//                        logger.info("文件成功删除: {}", targetFilePath);
//                    } else {
//                        logger.error("文件删除失败: {}", targetFilePath);
//                    }
//                } else {
//                    logger.error("文件不存在于文件系统中: {}", targetFilePath);
//                }
//            } catch (Exception e) {
//                logger.error("文件删除过程中发生错误, 文件ID: {}, 用户ID: {}", one.getId(), user.getId(), e);
//            }

            Long pid = one.getPid();
            rabbitTemplate.convertAndSend(MqContants.VIDEO_EXCHANGE,MqContants.VIDEO_DELETE_KEY,pid);
        }
    }

    @Transactional
    @Override
    public void hideVideo(Long videoId) {
        UserInfo user = AppThreadLocalUtil.getUser();
        LambdaQueryWrapper<Video> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(Video::getPid,videoId);
        Video one = getOne(queryWrapper);
        if (one.getUserId().longValue() != user.getId()){
            throw new BusinessException("权限不足");
        }
        LambdaUpdateWrapper<Video> updateWrapper=new LambdaUpdateWrapper<>();
        updateWrapper.eq(Video::getPid,videoId);
        updateWrapper.set(Video::getIsDeleted,1);
        boolean update = update(updateWrapper);
        if (update){
            SearchArticleVo searchArticleVo=new SearchArticleVo();
            searchArticleVo.setPid(one.getPid());
            searchArticleVo.setId(one.getId());
            searchArticleVo.setTitle(one.getTitle());
            searchArticleVo.setGmtCreated(one.getGmtCreated());
            searchArticleVo.setUserId(one.getUserId().longValue());
            searchArticleVo.setNickName(one.getNickName());
            searchArticleVo.setDescription(one.getDescription());
            searchArticleVo.setIsDeleted(1);
            rabbitTemplate.convertAndSend(MqContants.VIDEO_EXCHANGE,MqContants.VIDEO_UPDATE_KEY,searchArticleVo);
        }
    }

    @Override
    public void showVideo(Long videoId) {
        UserInfo user = AppThreadLocalUtil.getUser();
        LambdaQueryWrapper<Video> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(Video::getPid,videoId);
        Video one = getOne(queryWrapper);
        if (one.getUserId().longValue() != user.getId()){
            throw new BusinessException("权限不足");
        }
        LambdaUpdateWrapper<Video> updateWrapper=new LambdaUpdateWrapper<>();
        updateWrapper.eq(Video::getPid,videoId);
        updateWrapper.set(Video::getIsDeleted,0);
        boolean update = update(updateWrapper);
        if (update){
            SearchArticleVo searchArticleVo=new SearchArticleVo();
            searchArticleVo.setPid(one.getPid());
            searchArticleVo.setId(one.getId());
            searchArticleVo.setTitle(one.getTitle());
            searchArticleVo.setGmtCreated(one.getGmtCreated());
            searchArticleVo.setUserId(one.getUserId().longValue());
            searchArticleVo.setNickName(one.getNickName());
            searchArticleVo.setDescription(one.getDescription());
            searchArticleVo.setIsDeleted(0);
            rabbitTemplate.convertAndSend(MqContants.VIDEO_EXCHANGE,MqContants.VIDEO_UPDATE_KEY,searchArticleVo);
        }
    }


    public static void union(String dirPath, String toFilePath, String fileName, boolean delSource) throws BusinessException {
        File dir = new File(dirPath);
        if (!dir.exists()) {
            throw new BusinessException("目录不存在");
        }
        File fileList[] = dir.listFiles();
        File targetFile = new File(toFilePath);
        RandomAccessFile writeFile = null;
        try {
            writeFile = new RandomAccessFile(targetFile, "rw");
            byte[] b = new byte[1024 * 10];
            for (int i = 0; i < fileList.length; i++) {
                int len = -1;
                //创建读块文件的对象
                File chunkFile = new File(dirPath + File.separator + i);
                RandomAccessFile readFile = null;
                try {
                    readFile = new RandomAccessFile(chunkFile, "r");
                    while ((len = readFile.read(b)) != -1) {
                        writeFile.write(b, 0, len);
                    }
                } catch (Exception e) {
                    logger.error("合并分片失败", e);
                    throw new BusinessException("合并文件失败");
                } finally {
                    readFile.close();
                }
            }
        } catch (Exception e) {
            logger.error("合并文件:{}失败", fileName, e);
            throw new BusinessException("合并文件" + fileName + "出错了");
        } finally {
            try {
                if (null != writeFile) {
                    writeFile.close();
                }
            } catch (IOException e) {
                logger.error("关闭流失败", e);
            }
            if (delSource) {
                if (dir.exists()) {
                    try {
                        FileUtils.deleteDirectory(dir);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }


    private void cutFile4Video(Long fileId, String videoFilePath) {
        //创建同名切片目录
        File tsFolder = new File(videoFilePath.substring(0, videoFilePath.lastIndexOf(".")));
        if (!tsFolder.exists()) {
            tsFolder.mkdirs();
        }
        final String CMD_TRANSFER_2TS = "ffmpeg -y -i %s  -vcodec copy -acodec copy -vbsf h264_mp4toannexb %s";
        final String CMD_CUT_TS = "ffmpeg -i %s -c copy -map 0 -f segment -segment_list %s -segment_time 30 %s/%s_%%4d.ts";

        String tsPath = tsFolder + "/" + Constants.TS_NAME;
        //生成.ts
        String cmd = String.format(CMD_TRANSFER_2TS, videoFilePath, tsPath);
        ProcessUtils.executeCommand(cmd, false);
        //生成索引文件.m3u8 和切片.ts
        cmd = String.format(CMD_CUT_TS, tsPath, tsFolder.getPath() + "/" + Constants.M3U8_NAME, tsFolder.getPath(), fileId);
        ProcessUtils.executeCommand(cmd, false);
        //删除index.ts
        new File(tsPath).delete();
    }

}
