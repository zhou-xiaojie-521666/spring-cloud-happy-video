package com.zhou.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhou.model.common.dtos.LoginDto;
import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.model.user.dto.RegisterDto;
import com.zhou.model.user.pojos.UserInfo;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public interface UserService extends IService<UserInfo> {
    ResponseResult login(LoginDto loginDto);

    void register(RegisterDto dto);

    UserInfo getUserByToken(String token);

    UserInfo getUserInfoById(Long id);

    UserInfo updataUserInfo(MultipartFile file, String nickName, String desc);

    boolean addAttention(Long bloggerId);

    boolean isFollowing(Long bloggerId);

    boolean unFollow(Long bloggerId);

    List<UserInfo> getFollowingList(Long bloggerId);

    long getFollowingCount(Long bloggerId);


    long getFansCount(Long bloggerId);
}
