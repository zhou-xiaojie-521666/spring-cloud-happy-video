package com.zhou.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhou.model.user.pojos.EmailCode;
import org.springframework.stereotype.Service;

@Service
public interface EmailCodeService extends IService<EmailCode> {
    public void sendEmailCode(String email,Integer type);

    public void checkCode(String email,String emailCode);
}
