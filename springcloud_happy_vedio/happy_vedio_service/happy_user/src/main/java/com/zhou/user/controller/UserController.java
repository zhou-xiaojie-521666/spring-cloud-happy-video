package com.zhou.user.controller;

import com.zhou.common.exception.BusinessException;
import com.zhou.model.Constants;
import com.zhou.model.common.dtos.LoginDto;
import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.model.enums.ResponseCodeEnum;
import com.zhou.model.user.dto.RegisterDto;
import com.zhou.model.user.pojos.UserInfo;
import com.zhou.user.service.EmailCodeService;
import com.zhou.user.service.UserService;
import com.zhou.utils.CreateImageCode;
import com.zhou.utils.StringTools;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/user")
@Api(value = "web端用户登录", tags = "ap_user", description = "web端用户登录API")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private EmailCodeService emailCodeService;

    @PostMapping("/login")
    @ApiOperation("用户登录")
    public ResponseResult login(@RequestBody LoginDto dto, HttpSession session,
                                HttpServletRequest request){

        if (!dto.getCode().equalsIgnoreCase((String) session.getAttribute(Constants.CHECK_CODE_KEY))) {
                throw new BusinessException("验证码错误");
        }
        return userService.login(dto);
    }

    @PostMapping("/register")
    @ApiOperation("用户注册")
    public ResponseResult register(@RequestBody RegisterDto dto, HttpSession session,
                                   HttpServletRequest request){

        try {
            if (!dto.getCheckCode().equalsIgnoreCase((String) session.getAttribute(Constants.CHECK_CODE_KEY_EMAIL))) {
                throw new BusinessException("验证码错误");
            }
            userService.register(dto);
            return ResponseResult.okResult(null);
        }finally {
            session.removeAttribute(Constants.CHECK_CODE_KEY_EMAIL);
        }

    }



    /**
     * 先得到验证码
     *
     * @return
     */
    @ApiOperation("获取验证码")
    @RequestMapping("/checkCode")
    public void checkCode(HttpServletResponse response, HttpSession session, Integer type) throws IOException {
        CreateImageCode vCode = new CreateImageCode(130, 30, 5, 10);
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        response.setContentType("image/jpeg");
        String code = vCode.getCode();
        //登录注册
        if (type == null || type == 0) {
            session.setAttribute(Constants.CHECK_CODE_KEY, code);
        } else {
            //获取邮箱发送的验证码
            session.setAttribute(Constants.CHECK_CODE_KEY_EMAIL, code);
        }
        vCode.write(response.getOutputStream());
    }

    @ApiOperation("发送qq邮箱验证码")
    @RequestMapping("/sendEmailCode/{email}/{type}")
    public ResponseResult sendEmailCode(HttpSession session,
                                        @PathVariable String email, @PathVariable Integer type) {
        try {
            if (StringTools.isEmpty(email) || type == null) {
                throw new BusinessException(ResponseCodeEnum.CODE_600);
            }
            emailCodeService.sendEmailCode(email, type);

            return ResponseResult.okResult(null);
        } finally {
            session.removeAttribute(Constants.CHECK_CODE_KEY_EMAIL);
        }

    }

    @ApiOperation("根据token获取用户信息")
    @RequestMapping("/getUserByToken")
    public ResponseResult getUserByToken(String token){
        UserInfo userByToken = userService.getUserByToken(token);
        return ResponseResult.okResult(userByToken);
    }

    @ApiOperation("根据Id获取用户信息")
    @RequestMapping("/getUserInfoById/{id}")
    public ResponseResult getUserInfoById(@PathVariable Long id){
        UserInfo userByToken = userService.getUserInfoById(id);
        return ResponseResult.okResult(userByToken);
    }

    @RequestMapping("/getUserById/{id}")
    public UserInfo getUserById(@PathVariable Long id){
        UserInfo userByToken = userService.getUserInfoById(id);
        return userByToken;
    }

    @RequestMapping("/updataUserInfo")
    public ResponseResult updataUserInfo(MultipartFile file,String nickName,String desc){
        UserInfo userInfo = userService.updataUserInfo(file, nickName, desc);
        return ResponseResult.okResult(userInfo);
    }


    /**
     * 本地的关注了别人
     * @param bloggerId
     * @return
     */
    @RequestMapping("/addAttention/{bloggerId}")
    public ResponseResult addAttention(@PathVariable Long bloggerId){
        boolean b = userService.addAttention(bloggerId);
        return b?ResponseResult.okResult(200):ResponseResult.okResult(201);
    }

    /**
     * 检查是否关注了
     * @param bloggerId
     * @return
     */
    @RequestMapping("/isFollowing/{bloggerId}")
    public ResponseResult isFollowing(@PathVariable Long bloggerId){
        boolean b = userService.isFollowing(bloggerId);
        return b?ResponseResult.okResult(200):ResponseResult.okResult(201);
    }


    /**
     * 取消关注
     * @param bloggerId
     * @return
     */
    @RequestMapping("/unFollow/{bloggerId}")
    public ResponseResult unFollow(@PathVariable Long bloggerId){
        boolean b = userService.unFollow(bloggerId);
        return b?ResponseResult.okResult(200):ResponseResult.okResult(201);
    }

    /**
     * 获取关注列表
     * @param bloggerId
     * @return
     */
    @RequestMapping("/getFollowingList/{bloggerId}")
    public ResponseResult getFollowingList(@PathVariable Long bloggerId){
        List<UserInfo> userInfoList = userService.getFollowingList(bloggerId);
        return ResponseResult.okResult(userInfoList);
    }

    /**
     * 获取关注数量
     * @param bloggerId
     * @return
     */
    @RequestMapping("/getFollowingCount/{bloggerId}")
    public ResponseResult getFollowingCount(@PathVariable Long bloggerId){
        long count = userService.getFollowingCount(bloggerId);
        return ResponseResult.okResult(count);
    }

    /**
     * 获取粉丝数量
     * @param bloggerId
     * @return
     */
    @RequestMapping("/getFansCount/{bloggerId}")
    public ResponseResult getFansCount(@PathVariable Long bloggerId){
        long count = userService.getFansCount(bloggerId);
        return ResponseResult.okResult(count);
    }



}
