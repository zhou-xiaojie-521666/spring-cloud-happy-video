package com.zhou.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhou.common.exception.BusinessException;
import com.zhou.model.Constants;
import com.zhou.model.user.pojos.EmailCode;
import com.zhou.model.user.pojos.UserInfo;
import com.zhou.user.config.WebConfig;
import com.zhou.user.mapper.EmailCodeMapper;
import com.zhou.user.mapper.UserMapper;
import com.zhou.user.service.EmailCodeService;
import com.zhou.utils.StringTools;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.mail.internet.MimeMessage;

import java.util.Date;

@Service
@Transactional
@Slf4j
public class EmailCodeServiceImpl extends ServiceImpl<EmailCodeMapper,EmailCode> implements EmailCodeService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private EmailCodeMapper emailCodeMapper;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private WebConfig webConfig;
    private static final Logger logger= LoggerFactory.getLogger(EmailCodeServiceImpl.class);

    @Override
    public void sendEmailCode(String email, Integer type) {
        if (type==0){
            //注册，type 0是注册 1是发送邮箱的验证码
            LambdaQueryWrapper<UserInfo> queryWrapper=new LambdaQueryWrapper<>();
            queryWrapper.eq(UserInfo::getEmail,email);
            UserInfo userInfo=userMapper.selectOne(queryWrapper);
            if (userInfo!=null){
                throw new BusinessException("邮箱已存在");
            }
        }
        String code= StringTools.getRandomString(Constants.LENGTH_5);
        sendEmailCodeDo(email,code);
        //将之前发送的验证码都设为不可用
        LambdaUpdateWrapper<EmailCode> updateWrapper=new LambdaUpdateWrapper<>();
        updateWrapper.eq(EmailCode::getEmail,email);
        updateWrapper.eq(EmailCode::getStatus,0);
        updateWrapper.set(EmailCode::getStatus,1);
        emailCodeMapper.update(null,updateWrapper);
        //插入
        EmailCode emailCode=new EmailCode();
        emailCode.setCode(code);
        emailCode.setEmail(email);
        emailCode.setStatus(Constants.ZERO);
        emailCode.setCreateTime(new Date());
        emailCodeMapper.insert(emailCode);
    }

    private void sendEmailCodeDo(String toEmail,String code){
        try {
            MimeMessage message=javaMailSender.createMimeMessage();
            MimeMessageHelper helper= new MimeMessageHelper(message,true);
            //邮件发送人
            helper.setFrom(webConfig.getSendUserName());
            //邮件收件人
            helper.setTo(toEmail);
            //设置邮件标题
            helper.setSubject("注册邮箱验证码");
            helper.setText("您的验证码为:"+code);
            helper.setSentDate(new Date());
            javaMailSender.send(message);
        } catch (Exception e) {
            logger.error("发送邮件失败",e);
            throw new BusinessException("邮件发送失败");
        }
    }

    @Override
    public void checkCode(String email, String emailCode) {
        LambdaQueryWrapper<EmailCode> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EmailCode::getEmail, email);
        queryWrapper.eq(EmailCode::getCode, emailCode);
        EmailCode ddInfo = this.emailCodeMapper.selectOne(queryWrapper);
        if (null == ddInfo) {
            throw new BusinessException("邮箱验证码不正确");
        }
        if (ddInfo.getStatus() != 0 || System.currentTimeMillis() - ddInfo.getCreateTime().getTime() > 1000 * 60 * Constants.LENGTH_5 * 3) {
            throw new BusinessException("邮箱验证码已经失效");
        }
        //都正常，重置一下验证码
        LambdaUpdateWrapper<EmailCode> updateWrapper=new LambdaUpdateWrapper<>();
        updateWrapper.eq(EmailCode::getEmail,email);
        updateWrapper.eq(EmailCode::getStatus,0);
        updateWrapper.set(EmailCode::getStatus,1);
        emailCodeMapper.update(null,updateWrapper);
    }
}
