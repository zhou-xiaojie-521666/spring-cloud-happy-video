package com.zhou.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhou.model.user.pojos.EmailCode;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EmailCodeMapper extends BaseMapper<EmailCode> {
}
