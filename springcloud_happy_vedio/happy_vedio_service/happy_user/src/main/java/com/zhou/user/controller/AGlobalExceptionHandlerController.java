package com.zhou.user.controller;


import com.zhou.common.exception.BusinessException;
import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.model.enums.ResponseCodeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import java.net.BindException;


@RestControllerAdvice
public class AGlobalExceptionHandlerController {

    private static final Logger logger= LoggerFactory.getLogger(AGlobalExceptionHandlerController.class);
    @ExceptionHandler(value = Exception.class)
    Object handleException(Exception e, HttpServletRequest request){
        logger.error("请求错误，错误地址{},错误信息",request.getRequestURI(),e);
        ResponseResult ajaxResponse=new ResponseResult();
        //404
        if(e instanceof NoHandlerFoundException){
            ajaxResponse.setCode(ResponseCodeEnum.CODE_404.getCode());
            ajaxResponse.setErrorMessage(ResponseCodeEnum.CODE_404.getMsg());
            ajaxResponse.setData(500);
        } else if (e instanceof BusinessException) {
            BusinessException biz=(BusinessException) e;
            ajaxResponse.setCode(biz.getCode()==null? ResponseCodeEnum.CODE_600.getCode() : biz.getCode());
            ajaxResponse.setErrorMessage(biz.getMessage());
            ajaxResponse.setStatus(500);
        }else if (e instanceof BindException || e instanceof MethodArgumentNotValidException){
            ajaxResponse.setCode(ResponseCodeEnum.CODE_600.getCode());
            ajaxResponse.setErrorMessage(ResponseCodeEnum.CODE_500.getMsg());
            ajaxResponse.setStatus(500);
        } else if (e instanceof DuplicateKeyException) {
            ajaxResponse.setCode(ResponseCodeEnum.CODE_601.getCode());
            ajaxResponse.setErrorMessage(ResponseCodeEnum.CODE_601.getMsg());
            ajaxResponse.setStatus(500);
        } else {
            ajaxResponse.setCode(ResponseCodeEnum.CODE_500.getCode());
            ajaxResponse.setErrorMessage(ResponseCodeEnum.CODE_500.getMsg());
            ajaxResponse.setStatus(500);
        }
        return ajaxResponse;
    }
}
