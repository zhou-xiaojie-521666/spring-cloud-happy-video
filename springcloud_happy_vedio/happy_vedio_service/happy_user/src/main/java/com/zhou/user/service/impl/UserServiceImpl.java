package com.zhou.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhou.common.exception.BusinessException;
import com.zhou.model.Constants;
import com.zhou.model.common.dtos.LoginDto;
import com.zhou.model.common.dtos.ResponseResult;
import com.zhou.model.enums.UserStatusEnum;
import com.zhou.model.user.dto.RegisterDto;
import com.zhou.model.user.pojos.UserInfo;
import com.zhou.model.vedio.pojos.Video;
import com.zhou.user.config.WebConfig;
import com.zhou.user.feign.UserClient;
import com.zhou.user.mapper.UserMapper;
import com.zhou.user.service.EmailCodeService;
import com.zhou.user.service.UserService;
import com.zhou.user.utils.RedisUtil;
import com.zhou.utils.AppJwtUtil;
import com.zhou.utils.StringTools;

import com.zhou.utils.thread.AppThreadLocalUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, UserInfo> implements UserService {

    @Autowired
    private EmailCodeService emailCodeService;

    @Autowired
    private WebConfig webConfig;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private UserClient userClient;


    @Override
    public ResponseResult login(LoginDto loginDto) {
        LambdaQueryWrapper<UserInfo> queryWrapper = new LambdaQueryWrapper<>();
        UserInfo userInfo = getOne(queryWrapper.eq(UserInfo::getEmail, loginDto.getEmail()));
        //如果查询到的用户是空或者密码不正确的话，返回错误
        if (userInfo==null|| !userInfo.getPassword().equals(StringTools.encodeMd5(loginDto.getPassword()))){
            throw new BusinessException("账号或者密码错误");
        }
        //验证用户的状态，如果用户的状态的禁用的状态，则返回错误
        if (userInfo.getIsDeleted().equals(1)){
            throw new BusinessException("账号被禁用");
        }

        //TODO 获取ip
//        String ipreal = getIp();
//        String ipAddress=getIpAdress(ip);
//        //new一个用户，修改用户最后登录的时间
//        UserInfo updateInfo=new UserInfo();
//        updateInfo.setLastLoginTime(new Date());
//        updateInfo.setLastLoginIp(ipreal);
//        updateInfo.setLastLoginIpAddress(ipAddress);
//        userInfoMapper.updateById(updateInfo);

        //登录成功后返回token
        String token = AppJwtUtil.getToken(userInfo.getId().longValue());
        Map<String,Object> map=new HashMap<>();
        map.put("token",token);
        userInfo.setPassword("");
        map.put("user",userInfo);
        return ResponseResult.okResult(map);
    }

    @Override
    public void register(RegisterDto dto) {
        LambdaQueryWrapper<UserInfo> queryWrapper=new LambdaQueryWrapper<>();
        //根据邮箱查询用户
        queryWrapper.eq(UserInfo::getEmail,dto.getEmail());
        UserInfo userInfo = getOne(queryWrapper);
        //如果查询到有数据，则说明已经注册了
        if (userInfo!=null){
            throw new BusinessException("邮箱已经注册");
        }

        //检查验证码
        emailCodeService.checkCode(dto.getEmail(), dto.getEmailCode());
        //没有错的话添加用户信息
        String userId= StringTools.getRandomNumber(Constants.LENGTH_5*2);
        UserInfo insertInfo=new UserInfo();
        insertInfo.setId(null);
        insertInfo.setNickName(dto.getEmail());
        insertInfo.setEmail(dto.getEmail());
        insertInfo.setPassword(StringTools.encodeMd5(dto.getPassword()));
        insertInfo.setGmtCreated(new Date());
        insertInfo.setIsDeleted(UserStatusEnum.DISABLE.getStatus());
        insertInfo.setAvatar("https://img0.baidu.com/it/u=771583120,1915458320&fm=253&app=138&size=w931&n=0&f=JPEG&fmt=auto?sec=1712854800&t=7e9723a59c43e21413bf72bc7e5eba83");
//        insertInfo.setLastLoginIp(getIp());
//        insertInfo.setLastLoginIpAddress(getIpAdress(null));
        save(insertInfo);


    }

    @Override
    public UserInfo getUserByToken(String token) {
        if (StringTools.isEmpty(token)){
            throw new BusinessException("token身份无效");
        }
        Claims claimsBody = AppJwtUtil.getClaimsBody(token);
        //是否是过期
        int result = AppJwtUtil.verifyToken(claimsBody);
        if(result == 1 || result  == 2){
            throw new BusinessException("token身份无效");
        }

        //获取用户信息
        Object userId = claimsBody.get("id");
        LambdaQueryWrapper<UserInfo> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(UserInfo::getId,userId);
        UserInfo userInfo = getOne(queryWrapper);
        userInfo.setPassword(null);
        if (userInfo==null){
            return null;
        }else {
            return userInfo;
        }

    }

    @Override
    public UserInfo getUserInfoById(Long id) {
        LambdaQueryWrapper<UserInfo> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(UserInfo::getId,id);
        UserInfo one = getOne(queryWrapper);
        one.setPassword(null);
        one.setIsDeleted(null);
        return one;
    }

    @Override
    public UserInfo updataUserInfo(MultipartFile file, String nickName, String desc) {
        UserInfo user = AppThreadLocalUtil.getUser();
        Long userId = user.getId();
        LambdaQueryWrapper<UserInfo> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(UserInfo::getId,userId);
        UserInfo userInfo = getOne(queryWrapper);
        String path=null;
        LambdaUpdateWrapper<UserInfo> updateWrapper=new LambdaUpdateWrapper<>();
        updateWrapper.eq(UserInfo::getId,userId);
        if (file != null && !file.isEmpty()) {
            String fileName = file.getOriginalFilename();
            String fileExtName = StringTools.getFileSuffix(fileName);
            if (!ArrayUtils.contains(Constants.IMAGE_SUFFIX, fileExtName)) {
                throw new BusinessException("文件类型错误");
            }
            path = copyFile(file);
            updateWrapper.set(UserInfo::getAvatar,path);
        }
        if (nickName!=null&& !nickName.isEmpty()){
            updateWrapper.set(UserInfo::getNickName,nickName);
        }
        if (desc!=null&& !desc.isEmpty()){
            updateWrapper.set(UserInfo::getDescription,desc);
        }
        update(null,updateWrapper);
        return userInfo;
    }

    @Override
    public boolean addAttention(Long bloggerId) {
        UserInfo user = AppThreadLocalUtil.getUser();
        boolean b = redisUtil.addAttention(bloggerId, user.getId());
        return b;
    }

    @Override
    public boolean isFollowing(Long bloggerId) {
        UserInfo user = AppThreadLocalUtil.getUser();
        boolean following = redisUtil.isFollowing(user.getId(), bloggerId);
        return following;
    }

    @Override
    public boolean unFollow(Long bloggerId) {
        UserInfo user = AppThreadLocalUtil.getUser();
        boolean b = redisUtil.unFollow(user.getId(), bloggerId);
        return b;
    }

    @Override
    public List<UserInfo> getFollowingList(Long bloggerId) {
        Set<Object> followingList = redisUtil.getFollowingList(bloggerId);
        List<Long> userIds = followingList.stream()
                .map(Object::toString)
                .map(Long::parseLong)
                .collect(Collectors.toList());
        List<UserInfo> userInfoList=new ArrayList<>();
        userIds.stream().map((item)->{
            UserInfo userInfo = userClient.getUserInfoById(item);
            userInfoList.add(userInfo);
            return item;
        }).collect(Collectors.toSet());
        return userInfoList;
    }

    @Override
    public long getFollowingCount(Long bloggerId) {
        long followingCount = redisUtil.getFollowingCount(bloggerId);
        return followingCount;
    }

    @Override
    public long getFansCount(Long bloggerId) {
        return redisUtil.getFansCount(bloggerId);
    }


    private String copyFile(MultipartFile file) {
        try {
            String fileName = file.getOriginalFilename();
            String fileExtName = StringTools.getFileSuffix(fileName);
            String fileRealName = StringTools.getRandomString(30) + fileExtName;
            String folderPath = webConfig.getProjectFolder() + Constants.FILE_FOLDER_FILE + Constants.FILE_FOLDER_TEMP;
            File folder = new File(folderPath);
            if (!folder.exists()) {
                folder.mkdirs();
            }
            File uploadFile = new File(folderPath + "/" + fileRealName);
            file.transferTo(uploadFile);
//            + "/"
            return "temp" +"/"+ fileRealName;
        } catch (Exception e) {
            throw new BusinessException("上传文件失败");
        }
    }
}
