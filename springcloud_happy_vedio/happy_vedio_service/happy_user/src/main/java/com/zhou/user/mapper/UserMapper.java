package com.zhou.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhou.model.user.pojos.UserInfo;


public interface UserMapper extends BaseMapper<UserInfo> {
}
