/*
 Navicat Premium Data Transfer

 Source Server         : mengnan
 Source Server Type    : MySQL
 Source Server Version : 50724 (5.7.24)
 Source Host           : localhost:3306
 Source Schema         : happy_vedio

 Target Server Type    : MySQL
 Target Server Version : 50724 (5.7.24)
 File Encoding         : 65001

 Date: 28/04/2024 15:26:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for video
-- ----------------------------
DROP TABLE IF EXISTS `video`;
CREATE TABLE `video`  (
  `pid` bigint(50) NOT NULL AUTO_INCREMENT,
  `id` bigint(50) NOT NULL,
  `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `file_size` bigint(255) NULL DEFAULT NULL,
  `file_md5` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  `tag_list` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '类别id',
  `open` tinyint(1) NOT NULL DEFAULT 0 COMMENT '公开/私密，0：公开，1：进好友可见，2：私密 默认为0',
  `cover` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `audit_status` int(11) NOT NULL DEFAULT 0 COMMENT '审核的id',
  `audit_queue_status` int(12) NOT NULL DEFAULT 0 COMMENT '审核队列状态',
  `start_count` bigint(20) NULL DEFAULT 0 COMMENT '点赞状态',
  `share_count` bigint(20) NULL DEFAULT 0 COMMENT '分享数量',
  `history_count` bigint(20) UNSIGNED NULL DEFAULT 0 COMMENT '观看数量',
  `favorites_count` bigint(20) NULL DEFAULT 0 COMMENT '收藏数量',
  `is_deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除，0：未删除，1：删除，默认为0',
  `gmt_created` datetime NULL DEFAULT NULL,
  `gmt_updated` datetime NULL DEFAULT NULL,
  `post_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '0 立刻审核 1：指定时间前审核发布',
  `detail_post_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '什么时间前审核发布',
  `status` tinyint(1) NULL DEFAULT NULL COMMENT '0:转码中 1转码失败 2:转码成功',
  PRIMARY KEY (`pid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1710506255256 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for video_share
-- ----------------------------
DROP TABLE IF EXISTS `video_share`;
CREATE TABLE `video_share`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `video_id` bigint(20) NULL DEFAULT NULL,
  `receive_id` bigint(20) NULL DEFAULT NULL,
  `send_id` bigint(20) NULL DEFAULT NULL,
  `gmt_created` datetime NULL DEFAULT NULL,
  `gmt_updated` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_video_id_ip`(`video_id`, `receive_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1784454796095270914 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for video_type
-- ----------------------------
DROP TABLE IF EXISTS `video_type`;
CREATE TABLE `video_type`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `video_id` bigint(20) NOT NULL,
  `type_id` bigint(20) NOT NULL,
  `is_deleted` tinyint(1) NULL DEFAULT 0,
  `gmt_created` datetime NULL DEFAULT NULL,
  `gmt_updated` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
