/*
 Navicat Premium Data Transfer

 Source Server         : mengnan
 Source Server Type    : MySQL
 Source Server Version : 50724 (5.7.24)
 Source Host           : localhost:3306
 Source Schema         : happy_comment

 Target Server Type    : MySQL
 Target Server Version : 50724 (5.7.24)
 File Encoding         : 65001

 Date: 28/04/2024 15:26:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for comment_cover
-- ----------------------------
DROP TABLE IF EXISTS `comment_cover`;
CREATE TABLE `comment_cover`  (
  `id` bigint(50) NOT NULL AUTO_INCREMENT,
  `cover` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片路径',
  `comment_id` bigint(20) NULL DEFAULT NULL COMMENT '评论的id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for video_comment
-- ----------------------------
DROP TABLE IF EXISTS `video_comment`;
CREATE TABLE `video_comment`  (
  `id` bigint(50) NOT NULL AUTO_INCREMENT,
  `reply_id` bigint(20) NULL DEFAULT NULL COMMENT '回复别人评论的id 第一个评论是0',
  `video_id` bigint(20) NULL DEFAULT NULL COMMENT '视频id',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '发布人的id',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评论内容',
  `post_time` datetime NULL DEFAULT NULL COMMENT '发布时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
