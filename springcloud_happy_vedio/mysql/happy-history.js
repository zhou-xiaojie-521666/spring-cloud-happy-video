/*
 Navicat Premium Data Transfer

 Source Server         : mongo
 Source Server Type    : MongoDB
 Source Server Version : 70007 (7.0.7)
 Source Host           : 10.7.96.150:27017
 Source Schema         : happy-history

 Target Server Type    : MongoDB
 Target Server Version : 70007 (7.0.7)
 File Encoding         : 65001

 Date: 09/05/2024 16:25:25
*/


// ----------------------------
// Collection structure for ap_associate_words
// ----------------------------
db.getCollection("ap_associate_words").drop();
db.createCollection("ap_associate_words");

// ----------------------------
// Collection structure for ap_user_search
// ----------------------------
db.getCollection("ap_user_search").drop();
db.createCollection("ap_user_search");
