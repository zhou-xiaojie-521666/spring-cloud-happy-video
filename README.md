# happy-video

#### 介绍
{**是一个仿作某音网页版的项目，目前有登录注册，上传视频，对视频进行点赞评论，用户间的关注和私信，视频的转发等功能。**}

#### 软件架构
软件架构说明
后端使用springcloud进行微服务架构，有六大模块，聊天模块，评论模块，视频模块，用户模块，搜索模块，用户行为模块
使用网关进行鉴权和负载均衡，使用es进行分布式搜索，使用feign进行模块间交互，使用rabbitmq进行消息传递和异步通信，使用redis进行数据缓存。
数据库使用mysql，mongodb
#### 技术使用
jdk1.8，springboot，springcloud，nacos，gateway，feign，elasticsearch，mybatisplus，mysql，redis，rabbitmq，mongodb，jwt，swagger
#### 项目演示
更多项目演示在哔哩哔哩视频：
首页![输入图片说明](springcloud_happy_vedio/shouye.png)
![输入图片说明](shouye1.png)
推荐 ![输入图片说明](springcloud_happy_vedio/tuijian.png)
关注![输入图片说明](springcloud_happy_vedio/image.png)
我的![输入图片说明](springcloud_happy_vedio/wode.png)
投稿![输入图片说明](springcloud_happy_vedio/tougao.png)
私信![输入图片说明](springcloud_happy_vedio/sixin.png)
搜索![输入图片说明](springcloud_happy_vedio/sousuo.png)
#### 安装教程
1.  打开虚拟机安装docker，在docker在虚拟机安装nacos，rabbitmq，elasticsearch，mongo
![虚拟机docker镜像列表](https://foruda.gitee.com/images/1715243004684701314/6958eef4_12339792.png "屏幕截图")
2.  在虚拟机运行nacos，redis，elasticsearch，rabbit后，打开虚拟机地址：8848配置好nacos，具体的配置文件在mysql中的nacos.yaml
![输入图片说明](https://foruda.gitee.com/images/1715243061460145836/048fe686_12339792.png "屏幕截图")
3.  创建需要的数据库，导入sql文件创建表
![mysql数据库](https://foruda.gitee.com/images/1715243086420524598/5dd53845_12339792.png "屏幕截图")
![mongo表](https://foruda.gitee.com/images/1715243103195366836/7bed07dd_12339792.png "屏幕截图")
4.  配置每一个服务的配置文件
4.  idea运行项目
![idea项目运行](image.png)
5.  如有兴趣，有问题可以联系vx：15838372640

#### 使用说明

1.  电脑要有虚拟机，并且可以上网，虚拟机要有docker
2.  要有maven环境






